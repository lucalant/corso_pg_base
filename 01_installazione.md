---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio-Febbraio, 2025
keywords: [PostgreSQL, PostGIS, Open Source, GFOSS]

---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

# Installazione PostgreSQL

PostgreSQL è disponibile per diversi sistemi operativi: Linux (tutte le distribuzioni), Windows (Win2000 SP4 and later), FreeBSD,  OpenBSD, NetBSD, Mac OS X, AIX, HP/UX, IRIX, Solaris, Tru64 Unix,  and UnixWare.

- Per sfruttare al meglio tutte le potenzialità di PostgreSQL è consigliabile l'utilizzo su Linux in quanto abbiamo:
  Amministrazione più semplice (anche per le espensione e le dipendenze: PostGIS, ma anche  GEOS, GDAL, PL/R)

- Migliori performance

- Maggiori possibilità di tuning

- Maggior stabilità del sistema

  

## Linux

Tutte le principali distribuzioni linux contengono nei propri repository i pacchetti PostgreSQL. Sulle versioni debian-like per installarlo basta utilizzare i seguenti comandi:

```sh
sudo apt-get install postgresql
sudo apt-get install postgis
```


Nel caso si voglia installare una versione differente rispetto a quella proposta dalla propria distribuzione è possibile aggiungere i repository necessari. Per le distribuzioni debian like:

```shell
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt update
sudo apt-get install postgresql
sudo apt-get install postgis

#per avere versioni specifiche
sudo apt-get install postgresql-13
sudo apt-get postgresql-13-postgis-3
```


## Windows

riferimenti: https://www.geo-hub.net/come-installare-postgresql-e-postgis-su-windows		

Scaricare il pacchetto **PostgreSQL** sul sito di *EnterpriseDB* al seguente link [www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows)

Una volta scaricato il pacchetto avviamo l’installazione

![img](01_installazione.assets/PG-1.png)

Selezioniamo la directory di installazione, solitamente *C:\Programmi\PostgeSQL\version*

![img](01_installazione.assets/PG-2.png)

Selezioniamo le componenti che andremo ad installare, in questo caso tutte e quattro quelle disponibili: **PostgreSQL**, **pgAdmin4** *(software per la gestione dei db di PostgreSQL)*, **StackBuilder** *(ci permetterà di installare PostGIS in due semplici click*) e **Command Line Tools**

![img](01_installazione.assets/PG-3.png)

Selezioniamo la directory in cui verranno salvati i ***“dati”*** dei database che creeremo

![img](01_installazione.assets/PG-5.png)

Ora ci chiederà di scegliere una password per l’utente ***postgres*** che sarà il utente con i privilegi su di tutti i database che andremo a creare in futuro

![img](01_installazione.assets/PG-6.png)

)Successivamente ci chiederà di settare la porta sulla quale girerà il nostro ***PostgreSQL***, solitamente viene utilizzata la **5432**

![img](01_installazione.assets/PG-7.png)

In ***“****Select the locale to be used by the new database cluster.”*** lasciamo **[Default Locale]** e andiamo avanti avviando l’installazione di PostgreSQL

![img](01_installazione.assets/PG-8.png)

Completata l’installazione, lasciamo il flag su **Stack Builder** e clicchiamo Finish

![img](01_installazione.assets/PG-9.png)

Ora si avvierà lo ***Stack Builder***, nella prima scheda selezioniamo **PostgreSQL**

![img](01_installazione.assets/PG-10.png)

Nella schermata successiva estendiamo la voce ***Spatial Extensions*** e spuntiamo **PostGIS** 

![img](01_installazione.assets/PG-11.png)

Nella schermata successiva è importante **NON SPUNTARE** la voce ***skip installation*** e andiamo avanti

![img](01_installazione.assets/PG-13.png)

Ora si aprirà l’installazione di **PostGIS
** In ***“Select components to install”*** selezioniamo, oltre a PostGIS, ***“Create spatial database”***

![img](01_installazione.assets/PG-14.png)

Nella schermata successiva ci chiederà l’user e metteremo ***postgres***, la password quella scelta in precedenza e la porta su cui abbiamo deciso di installare **PostgreSQL** (di default 5432).
 Inserite queste credenziali e cliccato su **Next**, ci chiederà di dare un nome al database spaziale di demo che verrà creato

![img](01_installazione.assets/PG-15.png)

Clicchiamo su **Install** e avviamo l’installazione.

Ci chiederà di installare alcune librerie come GDAL_DATA noi diamo ***Yes***

## MAC

Anche per Apple MAC sono disponibili i pacchetti EDB che permettono l'installazione di PostgreSQL e PostGIS in modo facile e guidato.

- [ Scarica l'installer](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) certificato da EDB pe tutte le versioni supportate.
- Segui le istruzioni per windows

Esiste anche un pacchetto che non necessita installazione [postgresql.app](https://postgresapp.com/) che comprende PostgreSQL e PostGIS.

In questo caso esegui queste operazioni: 

- Scarica il pacchetto da qui https://postgresapp.com/

- sposta il pacchetto nella cartella applicazioni

- Fai doppio click sul pacchetto

- Nel caso si volgia includere anche la CLI (command line interface) ocorre aggiungere il percorso al PATH di sistema

  ```bash
  sudo mkdir -p /etc/paths.d &&
  echo /Applications/Postgres.app/Contents/Versions/latest/bin | sudo tee /etc/paths.d/postgresapp
  ```

  A questo punto è presente un server postgreSQL con le seguenti caratteristiche:

  | Host           | localhost              |
  | -------------- | ---------------------- |
  | Port           | 5432                   |
  | User           | your system user name  |
  | Database       | same as user           |
  | Password       | none                   |
  | Connection URL | postgresql://localhost |



# PGAdmin

Finita l’installazione avviamo **PgAdmin4**, che si avvierà sul nostro browser settato come predefinito.

![img](01_installazione.assets/PgAdmin-1.png)

Clicchiamo a sinistra su **Server** –> **PostgreSQL** e inseriamo la password dell’utente **postgres** *(settata durante l’installazione)

Inserita la password avremo accesso ai database, troveremo un DB dal  nome **postgres** e il DB **demo** di PostGIS, creato in precedenza durante l’installazione di quest’ultimo, con estensione spaziale e quindi con la possibilità di caricare dati cartografici.

![img](01_installazione.assets/PgAdmin_2.png)



# Creare un novo database

Per creare un nuovo DB con estensione PostGIS basta cliccare con il tasto destro su ***Database*** –> ***Creat****e*** –> ***Database…***

![img](01_installazione.assets/PgAdmin_3.png)

In **General** scegliamo il **nome per il nosto db** e un utente, mentre su **Definition** nella voce **Template** selezioniamo il database **PostGIS di demo**

![img](01_installazione.assets/PgAdmin_4.png)

 

# Configurazione 

## Utente di amministrazione

Postgres usa il concetto di ruoli per permettere l'autenticazione al database. Dopo l'installazione è disponibile un nuovo utente unix/linux chiamato postgres, a cui è associato il ruolo postgres sul db. Con tale utente è pertanto possibile accedere sia al database che a tutti i file presenti sul filesystem per la configurazione di Postgres.
E' possibile accedere all'utente con il comando:

```sql
sudo -i -u postgresql
```

occorre ancora impostare la password di amministrazione con il comando:

```sql
psql
>ALTER USER user_name WITH PASSWORD 'new_password';
>\q
```

su windows l'utente di amminstrazione viene configurato durante l'installazione

# Files di configurazione
Si trovano in:

- **linux**: Diepnde dalla distribuzione 
  - **Debian** e derivate: /etc/postgresql/[versione]/ [cluster]/;
  - **Redhat** e derivate: /etc/postgresql/[versione]/ [cluster]/;

- **Windows**: C:\Program Files\PostgreSQL\[versione]\data; 
- **OSX**: /usr/local/pgsql/data/postgresql.conf;

## postgresql.conf
Il file postgresql.conf È il file principale di configurazione di PostgreSQL, in cui puoi definire:

- **Porte e connessioni** (`port`, `listen_addresses`)
- **Performance e memoria** (`shared_buffers`, `work_mem`)
- **Logging** (`log_statement`, `log_directory`)

**Dove si trova?**

- **Linux/macOS:** `/etc/postgresql/`, `/var/lib/postgresql/data/`, o `/usr/local/pgsql/data/`
- **Windows:** `C:\Program Files\PostgreSQL\<version>\data\`



Per poter lavorare in rete occorre attivare l'ascolto via  TCP/IP:

```
# - Connection Settings -
listen_addresses = '*' # what IP interface(s) to listen on;

# defaults to localhost, '*' = any
port = 5432
```

Se non si usa l’asterisco (o l’indirizzo dell’interfaccia di rete del server) non ci si può connettere via TCP/IP.

## pg_hba.conf

E' il firewall di postgreSQL. In questo file si determinano gli accessi consentiti (host-based access). Esempio:

```
Database administrative login by Unix domain socket
local   all             postgres                                peer

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5

```



Qui trovate la documentazione: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html

Dopo avere effettuato le modifiche è necessario farle recepire al server attraverso un reload o un restart

da riga di comando linux:

```sh
sudo pg_ctlcluster [verision] [cluster] reload/restart
```

da sql:

```sql
SELECT pg_reload_conf();
```

