---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio-Febbraio, 2025
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

# corso_PG_base

Corso PostgreSQL livello base

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:4347566b082260d37dfee6c3b1024add?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:4347566b082260d37dfee6c3b1024add?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:4347566b082260d37dfee6c3b1024add?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/lucalant/corso_pg_base.git
git branch -M main
git push -uf origin main
```

