---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: 15 Gennaio, 2024
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

# Corso PostgreSQL / PostGIS

## Backup:

Il modo generale di fare il backup/restore da linea di comandi è il seguente. 

```
pg_dump -U utente database -f dumpfilename.sql
```

E possiible effettuare il backup da pgadmin posizionandosi sull'oggetto di cui si vuole fare il backup -> tasto dx -> backup. Le maschere permettono di scegliere le opzioni di backup in modo guidato. 

![image-20230217084933832](08_backup_e_restore.assets/image-20230217084933832.png)



![image-20230217085017061](08_backup_e_restore.assets/image-20230217085017061.png)

Esistono diversi **formati di backup**:

- PLAIN: Crea  un file di script SQL in testo normale (impostazione predefinita).

- CUSTOM: Crea un archivio in formato personalizzato adatto per l'input in pg_restore. Insieme al formato di output della directory, questo è il formato di output più flessibile in quanto consente la selezione manuale e il riordino degli elementi archiviati durante il ripristino. Anche questo formato è compresso per impostazione predefinita.

- DIRECTORY: Produce un archivio in formato directory adatto per l'input in pg_restore. Questo creerà una directory con un file per ogni tabella e blob scaricato, oltre a un cosiddetto file Table of Contents che descrive gli oggetti scaricati in un formato leggibile dalla macchina che pg_restore può leggere. Un archivio in formato directory può essere manipolato con strumenti Unix standard; ad esempio, i file in un archivio non compresso possono essere compressi con lo strumento gzip. Questo formato è compresso per impostazione predefinita e supporta anche i dump paralleli.

- TAR: Produce un archivio in formato tar adatto per l'input in pg_restore. Il formato tar è compatibile con il formato directory: l'estrazione di un archivio in formato tar produce un archivio in formato directory valido. Tuttavia, il formato tar non supporta la compressione. Inoltre, quando si utilizza il formato tar, l'ordine relativo degli elementi di dati della tabella non può essere modificato durante il ripristino.



Da linea di comando si può scegliere il formato (come le altre opzioni) utilizzando i parametri. In questo caso per scegliere il formato custom occorre utilizzare il parametro -F

```
pg_dump -U utente database -Fc dumpfilename.dump
```



## Restore:

Il ripristino di un database (restore) può essere fatto in modo differenti a seconda del formato scelto per il backup.

Nel caso venga usato il formato plain è possibile utilizzare il comando psql 

```
psql -U utente -d database -f dumpfilename.sql
```

Anche in questo  caso è possibile fare il restore direttamente da pg_admin copiando il file di backup all'interno di una finestra sql. Il backup infatti è composto completamente da comandi SQL.

 il formato custom permette maggiore flessibilità in fase di ripristino e richiede per il restore di utilizzare  pg_restore 

E' possibile effettuare il restore con il seguente comando

```sh
pg_restore -d dbname dumpfilename.dump 
```



## Opzioni avanzate di backup 

E' possibile fare il backup di tutti i database presenti in un cluster:

```
pg_dumpall > all.sql
```

Per fare il backup di una tabella o di uno schema specifici:

```
pg_dump --table tabella -U utente database -f onlytable.sql
pg_dump --schema schema_name -U utente database -f onlyschema.sql
```

Per fare il backup di un database di un server locale e fare il restore in un server remoto:

```
pg_dump database | psql -h server database
```



## Schedulare il backup 

Le operazioni di backup/restore si possono anche effettuare in automatico, con il comando cron:

```
crontab -e
05 03 * * *
 pg_dumpall > /home/user/all.sql
```

