---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio-Febbraio, 2025
keywords: [PostgreSQL, PostGIS, Open Source, GFOSS]
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

# Capitolo 4 - Gestire i dati

## Cos'è un database
_Con base di dati (o banca dati, a volte abbreviato con la sigla DB dall'inglese database) in informatica si indica un **insieme di dati strutturati** ovvero omogeneo per contenuti e formato, memorizzati in un computer, rappresentando di fatto la versione digitale di un archivio dati o schedario._ (https://it.wikipedia.org/wiki/Base_di_dati)

La biblioteca, l'elenco telefonico, l'anagrafe, gli strumenti social, le app di messaggistica si basano su un database. 
Anche i siti web hanno alle spalle un database dove vengono memorizzati gli utenti, le pagine, gli accessi, gli stili di rappresentazione....

Per il funzionamento di un database sono necessarie 3 componenti:
- **hardware**: un dispositivo semplice smartphone o un sistema complesso come uno o più server in cloud;
- **software** di gestione e accesso: PGAdmin, PSQL, pagine web PHP/Javascript, QGIS....
- **dati**: organizzati in tabelle costituite da righe (i record) e colonne (o campi) 

Caratteristica importante è che le colonne/campi delle tabelle hanno una tipologia definita: numerico, stringa, data, geometria...

I database sono di molti tipi: relazionali, gerarchici, a grafo, NoSQL... 
Postgres è relazionale e può essere usato anche con un approccio NoSQL.

I **database NoSQL** vengono definiti in modo intercambiabile come **"non relazionali", "DB NoSQL" o "non SQL"** per evidenziare il fatto che possono gestire enormi volumi di dati non strutturati e in rapida evoluzione in modi diversi rispetto a un database relazionale (SQL) con righe e tavole.
Le tecnologie NoSQL esistono dagli anni '60, sotto vari nomi, ma stanno godendo di un'impennata di popolarità man mano che il panorama dei dati cambia e gli sviluppatori devono adattarsi per gestire l'enorme volume e la vasta gamma di dati generati dal cloud, dai dispositivi mobili, dai social media, e grandi dati.
Dai tweet virali delle celebrità alle informazioni salvavita contenute nelle cartelle cliniche elettroniche, nuovi dati e tipi di dati vengono generati a un ritmo vertiginoso. 

I database NoSQL si sono evoluti per aiutare gli sviluppatori a creare rapidamente sistemi di database per archiviare le nuove informazioni e renderle immediatamente disponibili per la ricerca, il consolidamento e l'analisi. https://azure.microsoft.com/en-us/overview/nosql-database/

I database possono essere orientati ad un **singolo utente o per multiple utenze** contemporanee.

I database dispongono di una serie di comandi, che ne permettono:
- la gestione: creazione di database, tabelle, indici, viste, degli utenti;
- l'organizzazione: creazione di relazioni, trigger, funzioni;
- linguaggio di programmazione: funzioni e comandi per operare con i dati contenuti nelle tabelle.

Il **linguaggio SQL Standard Query Language**, è uno standard (standard is ISO/IEC 9075 "Database Language SQL") che poi ha un certo numero di _dialetti_.

I databases più noti sono:
- Oracle
- PostreSQL
- MYSQL
- MongoDB
- Redis
- Elasticsearch

## Tipi di dati di PostgreSQL

Fondamentalmente **PostgreSQL** gestisce quattro tipologie  di dato di base: 
- le stringhe;

- i valori numerici; 

- le date;

- i valori booleani o  logici. 

La capacità di distinguere tra tipi diversi è particolarmente  importante, dato che consente di ottimizzare l'allocazione dei dati  risparmiando spazio e velocizzando le operazioni di elaborazione.

### Numerici

I tipi di dato **numerici** si riferiscono a due principali sottocategorie: i numeri **interi** e i numeri **decimali**, questi ultimi sono detti anche numeri in "**virgola mobile**".
Nello specifico, riportiamo alcune tipologie di dato **intero**:

- **INTEGER**: numero intero costituito al massimo da 9 cifre (4 bytes);
- **SMALLINT**: numero intero per definizione più piccolo (2 bytes) di quello esprimibile tramite il tipo **INTEGER**;
- **BIGINT:** numero intero costituito al massimo da 19 cifre (8 bytes).

Per quanto riguarda invece i **decimali** elenchiamo:

- **FLOAT**: espresso singolarmente indica un numero in virgola mobile; **FLOAT**(*n*): indica invece un numero in virgola mobile della lunghezza di *n* bit. Il termine **numero in virgola mobile** è metodo di rappresentazione approssimata dei [numeri reali](https://it.wikipedia.org/wiki/Numero_reale) e di elaborazione dei dati molto prestante in termini di velocità nelle [operazioni matematiche](https://it.wikipedia.org/wiki/Operazioni_aritmetiche) e di spazio occupato.
- **REAL**: è anch'esso un numero in virgola mobile ma espresso in teoria con maggiore precisione rispetto a **FLOAT **(64 bit).
-  **DOUBLE PRECISION**: è un numero in virgola mobile in pratica equivalente al tipo **REAL**.

| Name     | Storage Size | Description         | Range            |
| -------- | ------------ | ------------------- | ---------------- |
| smallint | 2 bytes      | small-range integer | -32768 to +32767 |
|integer|	4 bytes|	typical choice for integer| -2'147'483'648 to +2'147'483'647 |
|bigint|	8 bytes|	large-range integer|	-9223372036854775808 to +9223372036854775807|
|decimal|	variable|	user-specified precision, exact| up to 131'072 digits before the decimal point; up to 16'383 digits after the decimal point |
|numeric|	variable|	user-specified precision, exact| up to 131'072 digits before the decimal point; up to 16'383 digits after the decimal point |
|real|	4 bytes|	variable-precision, inexact|	6 decimal digits precision|
|double precision|	8 bytes|	variable-precision, inexact|	15 decimal digits precision|


---

### Stringa

I tipi di dato stringa si riferiscono a sequenze di caratteri, per  stringa infatti si intende un insieme di caratteri composto da numeri che vanno da 0 a 9, da lettere dell'alfabeto, da spazi vuoti,  punteggiatura, simboli e caratteri speciali. Nello stesso modo  un'immagine, un video o un Mp3 sono delle sequenze di caratteri che  restituiscono un output multimediale.

Riportiamo alcuni esempi di dato stringa:

- **CHAR**: indica un singolo carattere; **CHAR**(*n*) indica invece una stringa della _lunghezza fissa_ di *n* caratteri.
- **VARCHAR**(*n*): indica una stringa di _lunghezza variabile_ composta al massimo da *n* caratteri.

Nel primo caso l'occupazione dello spazio disco sarà di *n* caratteri nel secondo l'occupazione su disco sarà variabile da 0 a n caratteri.

### Date

Le date vengono viste da **PostgreSQL** come se fossero un tipo a se stante, non un numero nè una stringa; ciò  consente di operare calcoli cronologici con un alto livello di  precisione, quasi come se l'**ORDBMS** avesse "coscienza" del tempo che passa.
Per quanto riguarda questa tipologia segnaliamo:

- **DATE**: indica una data intera completa di giorno, mese ed anno.
- **TIMESTAMP**: indica un'informazione contenente data e ora.
- **TIME**: indica un orario completo di ore, minuti ed secondi.
- **INTERVAL**: indica un intervallo di tempo.

### Booleani

I valori booleani rispondono alla logica binaria 0/1 oppure TRUE/FALSE, da cui abbiamo il valore **BOOLEAN** che può assumere soltanto uno tra questi due valori.**5. Rappresentazione dei dati in PostgreSQL**.

Esistono poi molti altri tipi di dati https://www.postgresql.org/docs/14/datatype.html

---

## Tipi di dati spaziali PostGIS

L'estensione PostGIS https://postgis.net/ **estende i tipi di campi gestiti normalmente** aggiungendo quelli che consentono la memorizzazione di dati geografici. Il suo sviluppo inizia nel 2001 e la versione 1.0 è del 2005. Nel 2020 è uscita la versione 3. 

Permette di definire un campo/colonna con tipologie di base quali:

-  POINT (X Y) (X Y Z) (X Y Z M)
-  MULTIPOINT (insieme di punti)
-  LINESTRING (X Y, X Y, X Y) 
-  MULTILINESTRING (insieme di linee)
-  POLYGON (come le LINESTRING dove primo e ultimo punto coincidono)
-  MULTIPOLYGON (insieme di poligoni)
-  GEOMETRYCOLLECTION (insieme eterogeneo di geometrie)

Le caratteristiche dei campi geometrici sono definite dall’Open Geospatial Consortium (OGC). 

Oltre alle strutture dati che possono ospitare dati geografici sono state realizzate molte funzioni per:
- crearne di nuovi;
- trasformarli e esportarli;
- scoprirne le caratteristiche (dimensioni, lunghezze, superfici);
- funzioni di aggregazione/separazione.

### ESERCIZIO 3 - il campo geometria

- con lo strumento  che preferite caricate nello schema dati il layer staz.json

- provate le seguenti query

```sql
select id, "PROVINCIA", geom 
from dati.province_dd
where "PROVINCIA" = 'Torino'
```

```sql
select id, geom
from dati.staz
LIMIT 20
```

la clausola *LIMIT* limita ad uno specifico numero di record

- ora provate a modificare le query precedenti usando la funzione ST_ASTEXT

```sql
select id, "PROVINCIA", ST_ASTEXT(geom) 
from dati.province_dd
ORDER BY "PROVINCIA"
```

la clausola *ORDER BY* ordina il risultato della query in ordine crescente

*ORDER BY campo DESC* propone il risultato in ordine inverso


```sql
select id, ST_ASTEXT(geom) 
from dati.staz
LIMIT 20
```

DOMANDA: come si presenta il campo delle geometrie?

 <img title="" src="04_gestire_i_dati.assets/geometriewkt-16437467637591.png" alt="WKT" data-align="inline">

---

### ESERCIZIO 4 - creare un layer geografico

- in PGAdmin aprite il _Query Tool_

```sql
CREATE TABLE dati.punti
(
    gid serial,
    codice integer,
    nome character varying(50),
    descrizione character varying(500),
    geom geometry(Point,32632),
    CONSTRAINT punti_pkey PRIMARY KEY (gid)
);

CREATE TABLE dati.linee
(
    gid serial,
    codice integer,
    nome character varying(50),
    descrizione character varying(500),
    geom geometry(Linestring,32632),
    CONSTRAINT linee_pkey PRIMARY KEY (gid)
);

CREATE TABLE dati.poligoni
(
    gid serial,
    codice integer,
    nome character varying(50),
    descrizione character varying(500),
    geom geometry(MultiPolygon,32632),
    CONSTRAINT poligoni_pkey PRIMARY KEY (gid)
)


```

Nella creazione del campo **geom** abbiamo specificato 2 cose:
- featuretype: Point, Linestring, MultiPolygon
- SRID: sistema di coordinate di riferimento  https://epsg.org/search/by-name?sessionkey=n9i4x54qpd&searchedTerms=32632

---

- provate ad eseguire questa query

```sql
SELECT * FROM public.spatial_ref_sys
ORDER BY srid ASC 
```

## Indici 

Un indice è un metodo di ottimizzazione delle prestazioni che consente un recupero più rapido dei record. Un indice crea una voce per ogni valore visualizzato nelle colonne indicizzate.

Gli indici sono estremamente importanti per la grandi tabelle spaziali, perché consentono alle queries di recuperare rapidamente i records che stanno cercando. Dato che PostGIS viene usato spesso per grandi moli di dati, imparare come costruire ed usare gli indici è cruciale.

 <img title="" src="04_gestire_i_dati.assets/bbox.png" alt="WKB" data-align="inline">

PostGIS utilizza gli indici R-Tree, implementati sullo schema di indicizzazione GiST (Generalized Search Tree). R-Trees organizza i dati spaziali in rettangoli (bounding box) annidati per ricerche rapide.

 <img title="" src="04_gestire_i_dati.assets/rtree.png" alt="WKB" data-align="inline">

Se la nostra finestra di visualizzazione fosse il rettangolo rosso allora l'indice considererebbe solo i rettangoli A e B escludendo il C in quanto non è _toccato_. Successivamente l'indice permetterà di far disegnare solo le geometrie **g** e **f** che sono effettivamente nella finestra escludendo il resto, le geometrie **d**, **e** e **h** non verranno escluse poichè il _bounding box_ non interseca il rettangolo rosso. 

http://postgis.net/workshops/postgis-intro/indexing.html

Il corretto utilizzo degli indici può velocizza enormemente la velocità di esecuzione delle *query*, quando le tabelle contengono una grande mole di dati. Alcuni dei sistemi di importazione dei dati visti in precedenza permette di creare contestualmente gli indici in fase di importazione. Se non fosse presente alcun indice spaziale questo può essere creato  direttamente da SQL con il comando: 

```sql
CREATE INDEX comuni_gidx ON comuni
USING GIST (geom);
```

La clausola USING GIST dice a PostgreSQL di usare una struttura con indice generico (GIST) nel momento in cui questo viene creato. Questo è importante perchè il tipo di indice di default è B-tree. L’indice GIST indicizza appena la bounding box delle geometrie, dove l’indice B-tree indicizza tutta la geometria, che può essere un operazione molto pesante.

https://blog.crunchydata.com/blog/the-many-spatial-indexes-of-postgis

## Chiave primaria

PostreSQL come ogni database relazionale necessita di una chiave primaria per identificare i record all'interno di una tabella.  La  **chiave primaria (primary key) può essere definita su più campi (colonne)** e la definizione è analoga a quella del vincolo unico, basta  specificare l'elenco delle colonne per le quali desideri che sia  soddisfatto il vincolo di univocità, ad esempio in questo modo:

```sql
CREATE TABLE MyTable(
    col1 integer,
    col2 integer,
    col3 integer,
    PRIMARY KEY (col1, col3)
);
```

Questo significa che sia la colonna 1 che la 3 sono usate come  identificatore univoco per le righe della tua tabella, essa potrà avere  più righe con col1 o col3 uguali ma solo una con una specifica coppia di valori per col1 e col3.

---

### ESERCIZIO 5 - creare una chiave primaria

- in PGAdmin aprite il _Query Tool_

- date i seguenti comandi:

```sql
CREATE TABLE mySchema.MyTable(
    col1 integer,
    col2 integer,
    col3 integer,
    PRIMARY KEY (col1, col3)
);

INSERT INTO myschema.mytable(	col1, col2, col3)	VALUES (1, 2, 3);
INSERT INTO myschema.mytable(	col1, col2, col3)	VALUES (2, 4, 3);
INSERT INTO myschema.mytable(	col1, col2, col3)	VALUES (1, 100, 3);
```

DOMANDA: cosa accade? Perchè?

DOMANDA 2: dove è stata creata la tavola **mytable** ?

---

## Sequence


Una sequence è un tipo speciale di dati creato per **generare identificatori numerici univoci** nel [database PostgreSQL](https://www.ntchosting.com/postgresql/database.html). Utilizzate più spesso per la creazione di chiavi primarie artificiali, le sequenze sono simili ma non identiche a AUTO_INCREMENT in [MySQL](https://www.ntchosting.com/encyclopedia/databases/mysql/). Gli oggetti sequenza (noti anche come generatori di sequenza o semplicemente sequenze) sono single-[row](https://www.ntchosting.com/encyclopedia/scripting-and-programming/row/) [tables](https://www .ntchosting.com/encyclopedia/scripting-and-programming/table/) creato tramite un comando dalla riga di comando: CREATE SEQUENCE. Gli oggetti sequenza vengono utilizzati più spesso per la creazione di identificatori univoci tra le righe della tabella. La funzione 'sequenza' offre un metodo multiutente semplice e sicuro per estrarre valori di sequenza da oggetti sequenza.

### ESERCIZIO 6 - Creare una sequence

Una sequence in PostgreSQL può essere creata facilmente usando il comando “CREATE SEQUENCE”.

```sql
CREATE SEQUENCE dati.phonebook_id_seq;

CREATE TABLE dati.phonebook(
    id INTEGER DEFAULT NEXTVAL('phonebook_id_seq'), 
    phone VARCHAR(32), 
    firstname VARCHAR(32),  
    lastname VARCHAR(32));

INSERT INTO dati.phonebook(phone, firstname,  lastname) 
VALUES('+1 123 456 7890', 'Easy', 'Peasy');

INSERT  INTO dati.phonebook(phone, firstname, lastname) 
VALUES('+1 124 457 7891', 'Lemon', 'Squeeze');  		
```


In postgreSQL è presente un tipo dati speciale chiamato **SERIAL** che può essere utilizzato per creare facilmente una chiave primaria autoinrcrementale.


| Name     | Storage Size | Description         | Range            |
| -------- | ------------ | ------------------- | ---------------- |
|smallserial|	2 bytes|	small autoincrementing integer|	1 to 32767|
|serial|	4 bytes|	autoincrementing integer|	1 to 2147483647|
|bigserial|	8 bytes|	large autoincrementing integer|	1 to 9223372036854775807|

```sql
CREATE TABLE table_name(
    id SERIAL,
    name VARCHAR NOT NULL
);
```

Il comando equivale a: 

```sql
CREATE SEQUENCE table_name_id_seq;

CREATE TABLE table_name (
    id integer NOT NULL DEFAULT nextval('table_name_id_seq')
);

ALTER SEQUENCE table_name_id_seq
OWNED BY table_name.id;Code language: SQL (Structured Query Language) (sql)
```



Creare un campo **gid, fid** di tipo **serial** e renderlo chiave primaria in un layer geografico è una buona pratica.



**ATTENZIONE !**

Recentemente il tipo dati SERIAL è stato rimpiazzato in PostgreSQL con **GENERATED ALWAYS AS IDENTITY**, più flessibile e maggiormente compatibile con lo standard SQL.

Quello che prima veniva definito come:

```sql
CREATE TABLE dati.poligoni
(
    gid SERIAL,
    codice integer
);
```

Adesso viene definito come:

```sql
CREATE TABLE dati.poligoni
(
    gid integer GENERATED ALWAYS AS IDENTITY,
    codice integer
);
```

---



## Valore NULL

Un caso a parte è rappresentato dal valore NULL.

NULL esprime un **valore non assegnato** ed è differente rispetto ad una stringa vuota **''** o dal valore **0** per un campo numerico.

Analizziamo alcuni casi dove è possibile utilizzare **NULL**.

Nella creazione di una tabella è possibile indicare se una colonna può o non può accettare campi non nulli. In questo caso non sarà possibile memorizzare un utente se non verranno indicati dei valori nei campi ID, NOME ed ETA. Il campo Indirizzo invece potrà rimanere **NULL**.

```sql
CREATE TABLE dati.utenti(
   id INT PRIMARY KEY     NOT NULL,
   nome           TEXT    NOT NULL,
   eta            INT     NOT NULL,
   indirizzo        CHAR(50)
);

INSERT INTO dati.utenti(id, nome, eta, indirizzo) VALUES (1, 'Paolo', 31, 'Via Pisa, 8');
INSERT INTO dati.utenti(id, nome, eta, indirizzo) VALUES (3, 'Francesco', 31, 'Via Assisi, 9');
INSERT INTO dati.utenti(id, nome, eta, indirizzo) VALUES (3, 'Stefania', 31, null);
```

In una SELECT, potremo selezionare i record in cui il campo INDIRIZZO è nullo 

```sql
SELECT * FROM dati.utenti WHERE indirizzo IS NULL;

SELECT * FROM dati.poligoni WHERE geom is null;
```
oppure non nullo

```sql
SELECT * FROM dati.utenti WHERE indirizzo IS NOT NULL;

SELECT * FROM dati.poligoni WHERE geom is not null;
```

Attenzione al valore NULL che può alterare il risultato di funzioni matematiche o sulle stringhe

```sql
select 10 + Null;

select 10.123 * Null;

select 'Ciao' || Null;
```
Il risultato di queste SELECT è sempre NULL!

Esistono funzioni che possono ignorare il valore NULL, ad esempio

```sql
select Concat(nome, ', ', eta ,  ', ', indirizzo)
FROM dati.utenti;


Paolo, 31, Via Pisa, 8                                       
Francesco, 31, Via Assisi, 9                                     
Stefania, 31, 
```

```sql
select nome || ', ' || eta ||  ', ' ||  coalesce(indirizzo,'N.D.')
FROM dati.utenti;

Paolo, 31, Via Pisa, 8
Francesco, 31, Via Assisi, 9
Stefania, 31, N.D.
```

La funzione **Concat** concatena stringhe e numeri e ignora il valore NULL.

La funzione **Coalesce** sostituisce il valore NULL con una stringa, in questo esempio con 'N.D.'.



### NULL e NaN


---

**NULL**:

- **Significato**: Rappresenta **l'assenza di un valore** (valore sconosciuto o non definito).
- **Tipo di dato**: Può essere usato con qualsiasi tipo di dato (stringhe, numeri, date, ecc.).
- **Confronto**: Non può essere confrontato direttamente con altri valori. Ad esempio, `NULL = NULL` restituisce `FALSE` perché `NULL` rappresenta l'incertezza.
- **Utilizzo**: È utile per rappresentare campi vuoti o informazioni mancanti.

**NaN**:

- **Significato**: Rappresenta **un valore numerico che non è definito o non è rappresentabile** (es. risultato di calcoli come `0/0` o `sqrt(-1)`).
- **Tipo di dato**: È specifico per tipi numerici a virgola mobile (`float4`, `float8`).
- **Confronto**: `NaN` non è uguale a nessun valore, incluso sé stesso (simile a `NULL` in questo senso). Tuttavia, puoi identificarlo usando funzioni come `isnan()` in PostgreSQL.
- **Utilizzo**: Serve per gestire errori matematici o numeri non rappresentabili nei calcoli.
