---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio-Febbraio, 2025
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

# Capitolo 6 - Analisi spaziali

Grazie a PostGIS, l'estensione spaziale di PostgreSQL è possibile utilizzare tutte le funzioni messe a disposizione dall'SQL ma in un contesto GIS.

Come abbiamo già detto PostGIS aggiunge tipi dati e funzioni specifiche a PostgreSQL. Vediamone qualcuna in azione. 

## Funzioni sul campo geom

### ESERCIZIO 7a - Un esempio di funzione spaziale

Le funzioni spaziali di postGIS generalmente operano  sul campo che contiene la geometria (geom).

qui trovate il manuale di postGIS: https://postgis.net/docs/manual-3.5/en/

e queste sono le funzioni disponibili

- da DBManager o da PGAdmin eseguite questa query

```sql
SELECT 
  id,
  denominazi,
  ST_Centroid(l.geom) as geom
FROM 
  dati.localita as l
```

Con la funzione [ST_Centroid](https://postgis.net/docs/ST_Centroid.html) ho calcolato il **centroide della geometria**: abbiamo trasformato il layer poligonale in elementi puntuali.

Per vedere il  vedere il risultato "spaziale" della mia query posso fare in due modi diversi.

1) **Utilizzare il visualizzatore interno di PGAdmin**

   ![image-20250205211808861](06_analisi_spaziali.assets/image-20250205211808861.png)

2) **Utilizzare DBManager da QGIS**

   ![image-20250205212138815](06_analisi_spaziali.assets/image-20250205212138815.png)

### ESERCIZIO 7b - Join (ma con qualche funzione PostGIS)

Possiamo estendere la query di precedente aggiungendo le potenzialità di SQL

- da DBManager o da PGAdmin eseguite questa query

```sql
SELECT 
  l.fid, 
  ST_Centroid(l.geom) as geom,
  l.loc2011 as codice_localita, 
  l.pro_com as codice_comune, 
  tl.descrizione as desc_tipo_localita,   
  l.denominazi as nome_localita,
  l.popres as popolazione_residente,
  l.maschi as maschi,
  l.popres - l.maschi as femmine,
  l.famiglie as numero_famiglie,
  l.abitazioni as numero_abitazioni,
  l.edifici as numero_edifici,
  l.shape_area as superficie_in_mq
FROM 
  dati.localita l
JOIN  dati.tipo_localita tl ON l.tipo_loc::integer = tl.tipo_loc::integer
  limit 10
```

Ci sono alcune novità:
- creiamo un dato che non esiste nelle tavole utilizzando una semplice formula matematica **l.popres - l.maschi as femmine**
- abbiamo messo tanti _alias_ la tavola è più leggibile
- abbiamo trasformato i campi **tipo_loc** delle due tabelle in **interi**. Il doppio **::** si riferisce a operatori che effettuano di **CAST** ovvero di trasformazioni di formato. https://www.postgresqltutorial.com/postgresql-cast/

### ESERCIZIO 7c - Funzioni in cascata

Ovviamente posso metter in cascata più funzioni spaziali in modo da creare operazioni complesse.

- da DBManager o da PGAdmin eseguite questa query

```sql
SELECT 
  id,
  denominazi,
  ST_Buffer(ST_Centroid(l.geom),100) as geom
FROM 
  dati.localita l
```

Ho trasformato il layer poligonale in uno puntuale con il centroide della località e nuovamente in uno poligonale creano un buffer di raggio 100m intorno al centroide.


---

## Viste

_Le viste sono un elemento utilizzato dalla maggior parte dei DBMS. Si tratta, come suggerisce il nome, di "modi di vedere i dati".
Una vista è rappresentata da una query (SELECT), il cui risultato può essere utilizzato come se fosse una tabella._
https://it.wikipedia.org/wiki/Vista_(basi_di_dati)

Le viste ci danno la possibilità di:
- dare una forma differente alle tavole con più o meno campi o con nomi differenti 
- creare nuove tavole prendendo le informazioni da più tavole relazionate
- il contenuto di una vista è **dinamico**: la vista segue i cambiamenti (contenuto di una o più colonne o aggiunta/rimozione dei record/righe) nelle tavole che alimentano la vista. 

---

### ESERCIZIO 8a - Creare una vista

Trasformiamo la query dell'esercizio precedente in una **vista** con l'aggiunta di questo comando:

**CREATE OR REPLACE VIEW dati.v_centroidi AS** 

```sql
CREATE OR REPLACE VIEW dati.v_centroidi AS 
SELECT 
  l.fid, 
  ST_Centroid(l.geom) as geom,
  l.loc2011 as codice_localita,
  l.loc2011 as nome_localita,
  l.pro_com as codice_comune
FROM 
  dati.localita l
```
- caricate i due layer in un progetto QGIS, aggiungete uno o più poligoni sul layer delle località.

DOMANDA: cosa accade quando salvate l'editing?

DOMANDA 2: potete fare editing sul layer dei centroidi?

DOMANDA 3: c'è qualcosa di strano nel Browser di QGIS? e nel DBManager?

Controllate la _feature type_ del campo **geom** della vista 

DOMANDA 4: navigate tra i dati con QGIS trovate qualche anomalia?



**ATTENZIONE**

Nel caso non fosse possibile salvare il layer della località occorre verificare che esista una chiave primaria e una sequence associata (campo contatore). 

E' possibile aggiungere il campo contatore con il comando:

```sql
ALTER TABLE table_name 
ALTER COLUMN column_name 
ADD GENERATED { ALWAYS | BY DEFAULT } AS IDENTITY { ( sequence_option ) }
```

nel nostro caso:

```sql
ALTER TABLE dati.localita 
ALTER COLUMN fid ADD GENERATED ALWAYS AS IDENTITY;
```

---

### ESERCIZIO 8b - Centroidi e centroidi

Trasformiamo la query dell'esercizio precedente in una nuova **vista** con l'aggiunta di questo comando:

**CREATE OR REPLACE VIEW dati.v_centroidi_2 AS** 

```sql
CREATE OR REPLACE VIEW dati.v_centroidi_2 AS 
SELECT 
  l.fid, 
  ST_PointOnSurface(l.geom)::geometry(Point,32632) AS geom, 
  l.loc2011 as codice_localita, 
  l.pro_com as codice_comune
FROM 
  dati.localita l
```

- caricate la nuova vista in QGIS

DOMANDA: ci sono differenze nelle geometrie delle due viste?

 <img title="" src="06_analisi_spaziali.assets/centroid.png" alt="centroidi" data-align="inline">

DOMANDA 2: come si presenta il campo **geom** nel Browser di QGIS? e nel DBManager?



1. Abbiamo utilizzato una nuova funzione spaziale che si chiama ST_PointOnSurface, simile alla precedente, ma in questo caso la funzione impone che il punto stia all'interno del poligono.
2. grazie a questa funzione di CAST (  [ST_PointOnSurface](https://postgis.net/docs/ST_PointOnSurface.html)(l.geom)**::geometry(Point,32632)** AS geom,  ) abbiamo detto a postgis che il nostro nuovo campo è di tipo puntuale è che il suo sitema di riferimento è il 32632



---

## Join spaziale (relazioni topologiche)

Fino ad ora abbiamo realizzato delle funzioni di **join** tra valori presenti nelle tabelle, ad esempio:

**WHERE l.tipo_loc::integer = tl.tipo_loc::integer**

possiamo utilizzare le funzioni che soddisfano **relazioni topologiche** tra le geometrie e che rispondo alle domande: è contenuto, tocca, interseca, non interseca ... https://postgis.net/docs/reference.html#idm11890

 <img title="" src="06_analisi_spaziali.assets/relazionispaziali.png" alt="relazioni spaziali" data-align="inline">

Notate che queste funzioni restituiscono un valore booleano TRUE o FALSE. 

 <img title="" src="06_analisi_spaziali.assets/msg372662734-23071.jpg" alt="twitter" data-align="inline" style="width:50%">

---

### ESERCIZIO 9a - Select con intersect

- Caricate in Postgres il layer dei comuni2019.gpkg che trovate nella cartella dei dati utilizzando Drag&Drop
- Rinominate la tavola in **comuni_piemonte** con il comando:

```sql
ALTER TABLE IF EXISTS dati.comuni2019 RENAME TO comuni_piemonte;
```

- Creiamo un vista più completa, che comprenda tutti i dati delle località, visto che ci serviranno nei prossimi esercizi.

```sql
DROP VIEW  dati.v_centroidi_2;
CREATE OR REPLACE VIEW dati.v_centroidi_2 AS 
SELECT 
    l.fid, 
    ST_Centroid(l.geom) as geom,
    l.loc2011 as codice_localita, 
    l.pro_com as codice_comune, 
    l.tipo_loc, 
    tl.descrizione as desc_tipo_localita,   
    l.denominazi as nome_localita,
    l.popres as popolazione_residente,
    l.maschi as maschi,
    l.popres - l.maschi as femmine,
    l.famiglie as numero_famiglie,
    l.abitazioni as numero_abitazioni,
    l.edifici as numero_edifici,
    l.shape_area as superficie_in_mq
  FROM 
    dati.localita l
  JOIN  dati.tipo_localita tl ON l.tipo_loc::integer = tl.tipo_loc::integer
```

- Voglio trova quali località sono relative al comune di Torino. 

  Non avendo una colonna con il nome del comune devo trovare un'altra soluzione. Con una intersezione spaziale con il layer dei comuni possiamo vedere quali punti cadono all'interno del comune di Torino.

```sql
SELECT 
 c.fid id_comune, l.fid id_localita, c.comune_nom, l.nome_localita, l.numero_famiglie::integer, l.numero_abitazioni
FROM 
 dati.comuni_piemonte c, 
 dati.v_centroidi_2 l
WHERE 
 ST_Intersects(l.geom, c.geom)
 AND c.comune_nom ilike 'Torino';
 
-- Stessa query utilizzando il JOIN 
select 
 c.fid id_comune, l.fid id_localita, c.comune_nom, l.nome_localita, l.numero_famiglie::integer, l.numero_abitazioni
from 
 dati.comuni_piemonte c 
 join dati.v_centroidi_2 l on ST_Intersects(l.geom, c.geom)
where 
 c.comune_nom ilike 'Torino';
```

La funzione [ST_intersects](https://postgis.net/docs/ST_Intersects.html) restituisce True o False e deve essere utilizzata nella WHERE. 



## Altri tipi di relazioni topologiche

Esistono molte funzioni per le relazioni topologiche. Queste sono quelle presenti nel manuale di postGIS:

- ST_3DIntersects — Tests if two geometries spatially intersect in 3D - only for points, linestrings, polygons, polyhedral surface (area)
- ST_Contains — Tests if every point of B lies in A, and their intcerchiamocerchiamoeriors have a point in common
- ST_ContainsProperly — Tests if every point of B lies in the interior of A
- ST_CoveredBy — Tests if every point of A lies in B
- ST_Covers — Tests if every point of B lies in A
- ST_Crosses — Tests if two geometries have some, but not all, interior points in common
- ST_Disjoint — Tests if two geometries have no points in common
- ST_Equals — Tests if two geometries include the same set of points
- **ST_Intersects** — Tests if two geometries intersect (they have at least one point in common)
- ST_LineCrossingDirection — Returns a number indicating the crossing behavior of two LineStrings
- ST_OrderingEquals — Tests if two geometries represent the same geometry and have points in the same directional order
- **ST_Overlaps** — Tests if two geometries have the same dimension and intersect, but each has at least one point not in the other
- ST_Relate — Tests if two geometries have a topological relationship matching an Intersection Matrix pattern, or computes their Intersection Matrix
- ST_RelateMatch — Tests if a DE-9IM Intersection Matrix matches an Intersection Matrix pattern
- **ST_Touches** — Tests if two geometries have at least one point in common, but their interiors do not intersect
- ST_Within — Tests if every point of A lies in B, and their interiors have a point in common



### ESERCIZIO 9b - ST_Touches 

Proviamone qualcuna. Voglio trovare tutti i comuni confinanti con quello di ASTI:


```sql
with a as (
	SELECT geom FROM dati.comuni
	where comune_nom ='ASTI'
)
SELECT fid,c.geom,comune_nom FROM dati.comuni c,a
where st_touches(c.geom,a.geom)
```

- Cosa capita se provo con il comune di TORINO ? Provate

Soluzione: Per due comuni c'è un punto che cade all'intern odel comune di TORINO:

Per verificarlo posso usare la funzione ST_Overlaps:

```sql
with a as (
	SELECT geom FROM dati.comuni
	where comune_nom ='TORINO'
)
SELECT fid,c.geom,comune_nom FROM dati.comuni c,a
where ST_Overlaps(c.geom,a.geom)
```

### ESERCIZIO 9c - Select con conteggio

Anche in questo caso posso unire le funzioni di base di SQL con quelle fornite da postGIS per fare analisi complesse.

**Domanda**: Voglio sapere quante località ci sono per ogni comune delle province di Asti e Biella.

- Eseguite la query

```sql
select
	c.comune_nom || ' (' || c.prov || ')' as comune,
	count(*) as numero_localita
from dati.comuni_piemonte c,
	dati.localita as l
where ST_Intersects(c.geom,
	ST_PointOnSurface(l.geom))
	and c.prov in ('at', 'bi') 
group by 1
order by 1;
```

Vediamo le caratteristiche principali della sintassi:

- il **||** permette di concatenare delle stringhe;
- usiamo la funzione **ST_PointOnSurface** direttamente nella **ST_Intersects** senza passare da una vista;
- la funzione **in** permette di indicare una lista di occorrenze, in questo caso vogliamo filtrare solo le province di Asti e Biella;
- nella **group by** e nella **order by** non indichiamo direttamente una colonna di una tavola ma il campo in posizione **1**


DOMANDA: quanto tempo ha impiegato il server a rispondere? 

DOMANDA 2: il layer ha un indice spaziale? Se non lo avesse createlo. 
Riprovate ad eseguire la query quanto tempo è passato per avere la risposta? 

- Modificate la query per avere la colonna **c.prov** in maiuscolo
(cercate una funzione di Postgres che trasforma le stringhe in maiuscolo)

- Modificate la query per avere il risultato in ordine di **numero_localita** dal più grande al più piccolo

---

### ESERCIZIO 9d - Select con conteggio 2

- Eseguite la query

```sql
select 
 c.comune_nom || ' (' || upper(c.prov) || ')' comune, 
 sum(l.maschi) as tot_maschi, 
 sum(l.femmine) as tot_femmine
from 
  dati.comuni_piemonte c, 
  dati.v_centroidi_2 l
where c.prov not in ('at', 'bi')  
and ST_Intersects(c.geom, l.geom) 
group by 1
order by 1;
```

Ci sono alcune novità:

- con **sum(l.maschi) as tot_maschi** e **sum(l.femmine) as tot_femmine** sommiamo i valori di due colonne della vista
- il **not in** permette filtrare solo quei comuni delle province che **non** sono Asti e Biella

CONSIGLIO: nelle clausole **WHERE** mettete sempre prima le condizioni che filtrano il vostro dataset

---

### ESERCIZIO 9e - Select con conteggio 3

. **Domanda:** Quanti kilometri di autostrade ci sono per ogni comune.

- Caricate in Postgres il layer autostrade.shp che trovate nella cartella dei dati

```sql
select 
 c.comune_nom, 
 count(*) as numero_archi,
 sum(ST_Length(ST_Intersection(c.geom, v.geom))/1000)::numeric(10,2) as lun_km
from 
  dati.comuni_piemonte c, 
  dati.autostrade v
where ST_Intersects(c.geom,v.geom) 
group by c.comune_nom
order by c.comune_nom;
```

Posso anche aggregare per più di un campo, ad esempio la tipologia di sede stradale:

```sql
select 
 c.comune_nom, el_str_sed, 
 count(*) as numero_archi,
 sum(ST_Length(ST_Intersection(c.geom, v.geom))/1000) as lun_km
from 
  dati.comuni_piemonte c, 
  dati.autostrade v
where ST_Intersects(c.geom,v.geom) 
group by c.comune_nom,el_str_sed
order by c.comune_nom,el_str_sed;
```

Ci sono alcune novità:

- la funzione **ST_Intersection** restituisce il tratto in comune tra il comune e l'arco delle autostrade
- la funzione **ST_Length** restituisce la lunghezza dell'arco in metri che poi dividiamo per 1000 per ottenere i km

---



## Proiezioni 

Con PostGIS è possibile gestire i sistemi di riferimento. Per vedere qual'è il sistema di riferimento di un recordset si può usare:

[ST_SRID](https://postgis.net/docs/ST_SRID.html): restituisce il sistema di riferimento di una geometria

```sql
SELECT distinct ST_SRID() 
FROM dati.staz;
```

Se non mettiamo un vicolo nella tavola è possibile inserire record con SRID differente



Anche in questo caso possiamo concatenare insieme più funzioni, comprese quelle di riproiezione. Per vederlo proviamo con questo esempio.

### ESERCIZIO 9f 

Domanda, quali sono le  località più vicine ad ogni stazione meteo nell'arco di 3Km ?

- Eseguite la query

```sql
select 
a_meteo_staz,s.strumento, l.nome_localita, 
st_distance(s.geom,  l.geom) d
from dati.staz s, 
dati.v_centroidi_2 l
where ST_Distance(s.geom, l.geom)  <= 3000
```

- Ci sono dei problemi ? 
- Se i sistemi di riferimento non corrispondono possiamo riproiettare le geometrie. Proviamo a risolvere con questa query

```sql
select 
a_meteo_staz,s.strumento, l.nome_localita, 
st_distance(st_transform(s.geom,32632), l.geom) d
from dati.staz s, 
dati.v_centroidi_2 l
where ST_Distance(st_transform(s.geom,32632), l.geom)  <= 3000
ORDER BY a_meteo_staz
```

- Il comando **st_transform** riproietta una geometria da un sistema di riferimento ad una altro.

- Possiamo anche creare un nuovo layer riproiettato e poi eseguire la query su quest'ultimo

```sql
-- creo la nuova tavola stazioni in EPSG:32632
create or replace table dati.staz_32632 as 
SELECT id,  a_meteo_staz,
ST_Transform(geom,32632)::Geometry(Point,32632) geom,
--ST_AsText(st_transform(geom,32632)::Geometry(Point,32632)),
a_indirizzo, strumento
FROM dati.staz;

-- eseguo nuovamente la query
select 
a_meteo_staz,s.strumento, l.nome_localita, 
st_distance(s.geom, l.geom) d
from dati.staz_32632 s, 
dati.v_centroidi_2 l
where ST_Distance(s.geom, l.geom)  <= 3000
```



---

### ESERCIZIO 9g - Creare nuove geometrie 

Cosa fa questa query?

```sql
CREATE TABLE dati.distanze AS
SELECT 
s.a_indirizzo, l.nome_localita, l.desc_tipo_localita,
ST_Distance(s.geom,  l.geom) d, 
ST_GeomFromText('LINESTRING (' || st_x(s.geom) || ' ' || st_y(s.geom) || ',' 
                               || st_x(l.geom) || ' ' || st_y(l.geom) || ')',32632) as geom
FROM dati.staz_32632 s, 
dati.v_centroidi_2 l 
WHERE l.desc_tipo_localita ILIKE '%abitato%'
AND ST_Distance(s.geom, l.geom)  <= 2000;
```

Ci sono alcune novità:

- l'operatore **ILIKE** permette di ricercare nelle stringhe usando dei caratteri _jolly_ **%**
In questo caso filtriamo tutte le righe **desc_tipo_localita** che contengono la stringa **abitato**
- la funzione **ST_Distance** la usiamo in due situazioni:
  1. nella clausola **WHERE** per filtrare solo le località che sono ad una distanza inferiore a 2000 metri
  2. nel calcolo della distanza tra la stazione e la località che verrà memorizzato nel campo **d**
- la funzione **ST_GeomFromText** ci permette di creare una nuova geometria a partire dalle coordinate della stazione e della località

### ESERCIZIO 9h - Nuove geometrie 2

Cosa fa questa query?

```sql
CREATE TABLE dati.distanze_autostrade AS
SELECT s.id, 
s.a_indirizzo, 
ST_Distance(s.geom,  a.geom) d, 
ST_GeomFromText('LINESTRING (' || st_x(s.geom) || ' ' || st_y(s.geom) || ',' 
                               || st_x(ST_PointOnSurface(a.geom)) || ' ' || st_y(ST_PointOnSurface(a.geom)) || ')',32632) as geom		
FROM 
dati.staz_32632 s, 
dati.autostrade a 
WHERE ST_Distance(s.geom, ST_PointOnSurface(a.geom))  <= 2000
and a.el_str_sed in ('in galleria','su ponte/viadotto/cavalcavia')
```

Stesso esercizio precedente ma questa volta usiamo il layer delle **autostrade** consideriamo solo il punto centrale dell'arco con la funzione **ST_PointOnSurface(a.geom)**



## Bonus: Aggregazioni tramite con partizioni 

In **PostgreSQL**, la clausola `OVER(PARTITION BY ...)` viene utilizzata con le **funzioni finestra (window functions)** per eseguire calcoli su un sottoinsieme di righe (partizioni) all'interno di una query, senza aggregarle completamente come farebbe una funzione di aggregazione classica (`GROUP BY`).

La Sintassi di base è la seguente:

```sql
SELECT 
    colonna1,
    colonna2,
    funzione_finestra() OVER (PARTITION BY colonnaX ORDER BY colonnaY) AS risultato
FROM tabella;
```

Vediamolo con un esempio pratico:

Abbiamo calcolato le distanze tra le località e le stazioni meteo. Adesso però voglio ordinare per ogni stazione meteo le località dalla più vicina alla più distante. Questa è la query: 

```sql
select 
a_meteo_staz,s.strumento, l.nome_localita, 
st_distance(st_transform(s.geom,32632), l.geom) distance,
rank() over (
				partition by a_meteo_staz 
				order by st_distance(st_transform(s.geom,32632), l.geom)
			) as rank 
from dati.staz s, 
dati.v_centroidi_2 l
where ST_Distance(st_transform(s.geom,32632), l.geom)  <= 3000
ORDER BY a_meteo_staz

```

E se volessi vedere solo le prime classificate? 

```sql
with a as (
select 
a_meteo_staz,s.strumento, l.nome_localita, 
st_distance(st_transform(s.geom,32632), l.geom) d,
rank() over (
				partition by a_meteo_staz 
				order by st_distance(st_transform(s.geom,32632), l.geom)
			) as rank 
from dati.staz s, 
dati.v_centroidi_2 l
where ST_Distance(st_transform(s.geom,32632), l.geom)  <= 3000
ORDER BY a_meteo_staz
)
select * FROM a 
where rank=1
```

Non posso usare le funzioni finestra direttamente nella where, quindi posso usare la sintassi con with

### **Funzioni comuni con `OVER(PARTITION BY ...)`**

- `SUM() OVER (...)` → Totale parziale o cumulativo
- `AVG() OVER (...)` → Media per partizione
- `RANK() OVER (...)` → Classifica le righe per ranking
- `DENSE RANK() OVER (...)` → Classifica le righe per ranking ma non salta i numeri in caso di pareggio
- `ROW_NUMBER() OVER (...)` → Assegna un numero progressivo a ogni riga nella partizione
- `LAG()/LEAD()` → Recupera il valore precedente o successivo rispetto alla riga corrente
