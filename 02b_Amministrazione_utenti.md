---
​---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio-Febbraio, 2024
keywords: [PostgreSQL, PostGIS, Open Source, GFOSS]

​---



---

# Gestione degli utenti  e dei permessi - introduzione

**OWNER**: In PostgreSQL, ogni oggetto di database ha solo un **proprietario** che, insieme ai ruoli di *superuser*, ha la capacità di alterare, eliminare e gestire l'oggetto stesso. 

**GRANTS**: Il proprietario dell'oggetto gestisce i privilegi sull'oggetto per altri ruoli concedendo i **privilegi**.

**ROLES, USERS e  GROUPS:** Le versioni precedenti di Postgres e alcuni altri sistemi DB hanno concetti separati di "**gruppi**" (a cui è concesso l'accesso agli oggetti del database) e "**utenti**" (che possono accedere e sono membri di uno o più gruppi).

Nelle versioni moderne di PostgreSQL, i due concetti sono stati fusi: un "**ruolo**" può avere la capacità di accedere, la capacità di "**ereditare**" da altri ruoli (come un utente che è membro di un gruppo o un gruppo che è membro di un altro gruppo) e l'accesso agli oggetti del database.

Per comodità, molti strumenti e manuali fanno riferimento a:

- "**USER**" o "ruolo di accesso" qualsiasi ruolo con autorizzazione di accesso (CAN LOGIN) 

- "**GROUP** " o "ruolo di gruppo", uqalisasi ruolo senza autorizzazione di accesso 

Questa è interamente una convenzione terminologica e per comprendere le autorizzazioni è sufficiente comprendere le opzioni disponibili durante la creazione dei ruoli e la concessione dell'accesso.

E' possibile profilare i diritti di accesso ai principali oggetti del database. Possono essere forniti i diritti a singoli utenti (**USERS**) o a gruppi (**ROLES**). Ogni oggetto ha differenti tipi di diritti:

DATABASE: Create, Temp, Connect
SCHEMA: Usage, Connect
TABLE, VIEW: Insert, Select, Update, Delete, Truncate, Trigger, References
FUNCTION: Execute

Dopo l'installazione esiste solo l’utente **postgres** che ha diritti di superutente. Si può usare questo utente per amministrare il database oppure crearne un altro con minori privilegi.

L'utente postgres <u>deve essere utilizzato unicamente per effettuare operazioni di amministrazione sul database</u> è altamente sconsigliabile utilizzarlo per l'accesso con altri scopi.  

```sql
CREATE USER utente PASSWORD '***';
--equivale a:
CREATE ROLE utente LOGIN PASSWORD '***';
```

per cambiare o aggiungere la password in un secondo momento:

```sql
ALTER ROLE utente password '***';
```

Per rimuoverlo:

```sql
DROP ROLE utente;
```

E' possibile creare dei gruppi a cui vengono assegnati gli utenti

```sql
CREATE ROLE gruppo;
```

Si aggiungono (e tolgono) gli utenti al gruppo:

```sql
GRANT gruppo TO utente1, utente2, ...;
REVOKE gruppo FROM utente;
```



## Creare database e schemi

In genere dopo l'installazione di base sono presenti alcuni database:

- postgres -> Database di amministrazione, deve sempre essere presente
- template0 -> non deve mai essere modificato per avere un template "pulito"
- template1 -> template utilizzato per la creazione dei nuovi DB

Per creare un nuovo database:

```sql
CREATE DATABASE test OWNER utente;
```

Quando viene creato un nuovo database viene semplicemente creata una copia del db template1.

Se vogliamo utilizzate template personalizzati possiamo usare il comando

```sql
CREATE DATABASE test OWNER utente TEMPLATE mytemplate;
```

All'interno di ogni database è possibile creare nuovi schemi e fornire i grant di accesso.

```sql
CREATE SCHEMA gis;
GRANT USAGE ON SCHEMA gis TO [user];
ALTER DATABASE corso SET search_path = gis, public;
```

L'ultimo comando indica l'ordine con cui vengono cercate le tavole negli schemi. (Opzionale, di default ogni database ha search_path = public ).



## Abilitare l'estensione PostGIS

Quando viene creato un nuovo database occorre abilitare in modo esplicito le estensioni desiderate. A partire da PostgreSQL 9.1, esiste il meccanismo delle estensioni, che facilita grandemente l’installazione di PostGIS. È sufficiente, come superuser del database:

```sql
CREATE EXTENSION postgis;
```

Questo comando copia tutte le funzioni, crea i tipi dati ed abilità il database all'utilizzo di PostGIS. 