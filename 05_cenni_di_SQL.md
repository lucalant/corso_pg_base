---

author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio-Febbraio, 2025
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

#  Introduzione al linguaggio SQL

SQL è un linguaggio per interrogare e gestire basi di dati mediante l'utilizzo di costrutti di programmazione denominati query. Con SQL si leggono, modificano, cancellano dati e si esercitano funzioni gestionali ed amministrative sul sistema dei database. La maggior parte delle implementazioni dispongono di interfaccia alla riga di comando per l'esecuzione diretta di comandi, in alternativa alla sola interfaccia grafica GUI.
https://it.wikipedia.org/wiki/Structured_Query_Language

## Sintassi specifica di PostgreSQL

L'SQL è un linguaggio case insensitive, questo vuol dire che se si scrive in maiuscolo o minuscolo la stessa espressione questa verrà interpretata nello stesso modo.

Esempio:

```sql
SELECT * FROM NOME_TABELLA
WHERE NOME_CAMPO = 'Corso Postgresql';

Select * from nome_TABELLA
WHERE NOME_Campo = 'CORSO Postgresql';

```

Anche gli identificatori di campo in PostgreSQL sono case insensitive. Questo vuol dire che comunque scriviamo il nome di un oggetto (un campo, una tavola ecc...) questo viene trattato come se fosse minuscolo.

```sql
CREATE TABLE dati.esempio (
gid serial,
dati text);

CREATE TABLE dati.ESEMPIO (
gid serial,
dati text);

CREATE TABLE dati.Esempio (
gid serial,
dati text);
```

La seconda e la terza query restituiscono un errore, perché in tutti e tre i casi si cerca di creare un a tavola con nome esempio. Per poter distinguere i caratteri maiuscoli e trasformare la sintassi in *case sensitive* occorre racchiudere l'identificatore tra doppi apici.

```sql
CREATE TABLE dati."esempio" (
gid serial,
dati text);

CREATE TABLE dati."ESEMPIO" (
gid serial,
dati text);

CREATE TABLE dati."Esempio" (
gid serial,
dati text);
```

I doppi apici permettono anche di utilizzare caratteri speciali quali lo spazio, il punto esclamativo, il punto e virgola.

```sql
CREATE TABLE dati."!ese mpio?" (
gid serial,
dati text);
```

E comunque buona norma **evitare di utilizzare i doppi apici per creare nomi di oggetti con i caratteri speciali**, La scrittura del codice diventerà più difficoltosa, di difficile lettura e più soggetta a potenziali errori.

E' possibile convertire con un'unica operazione tutti i nomi campi/colonne da maiuscolo a minuscolo

```sql
SELECT 'ALTER TABLE '|| table_schema ||'."'||table_name||'"'||' RENAME COLUMN '||'"'||column_name||'"'||' TO ' ||
lower(replace(replace(column_name,' ','_'),'-','_'))||';'
FROM information_schema.columns
WHERE lower(column_name) != column_name
AND table_schema='public'
order by table_schema, table_schema;
```

La query compone una nuova serie di altre query che devono essere copiate e lanciate in una nuova finestra dell'editor.
Attenzione: occorre modificare la penulitma riga della query in modo da ottenere solo le tavole dello schema richiesto (in questo caso public). Il risultato sarà qualcosa di simile:

```sql
ALTER TABLE public."staz" RENAME COLUMN "A_IDROMETRO" TO a_idrometro;
ALTER TABLE public."staz" RENAME COLUMN "A_PLUVIOMETRO" TO a_pluviometro;
ALTER TABLE public."staz" RENAME COLUMN "N_HS_ULTIMO" TO n_hs_ultimo;
ALTER TABLE public."staz" RENAME COLUMN "A_TERMOMETRO" TO a_termometro;
[....]
```



## La clausola SELECT

**SELECT** è l'istruzione SQL che permette di interrogare un database. L’interrogazione è la funzionalità più usata di un database e le clausole di cui dispone questa l’istruzione sono numerose. È  il  comando  principale  di  SQL  che  realizza  le  funzioni  di  linguaggio  per  le  interrogazioni  (Query Language):

- attivare le interrogazioni sulle relazioni
- implementare le operazioni relazionali per ottenere nuove tabelle.

La sintassi completa è la seguente:

```sql
SELECT [DISTINCT] nomi_colonne
FROM nome_tabella
[WHERE espressione_condizionale]
[GROUP BY lista_colonne]
[HAVING espressione_condizionale]
[ORDER BY lista_colonne]
```

*in maiuscolo, vengono indicati i comandi, in minuscolo i riferimenti e tra parentesi le parti di codice opzionali

- dopo SELECT:  nomi  delle  colonne  da  elencare  (per  indicare  tutti  gli  attributi  si  scrive  l'asterisco * accanto a Select )
- dopo FROM : il nome o i nomi delle tabelle  
- dopo WHERE:  la  condizione  da  controllare  sui  valori  delle  righe  (anche  più  condizioni  combinate con gli operatori AND, OR e NOT).  

```sql
SELECT * FROM tabella;
```

ATTENZIONE: la _tabella_ può essere formato dal **nomeschema.nometabella**

---

### ESERCIZIO 3 - prima SELECT

- Utilizzando DBManager oppure PGAdmin provate a scoprire quale versione di Postgres abbiamo installato

```sql
SELECT version();
```

- e quale versione di PostGIS

```sql
SELECT postgis_full_version();
```

---

- ora provate una query semplice sul layer delle province.

  

***n.b. Le query seguenti fanno riferimento al dataset "province" contenuto nel geopackage "datigeo.gpkg". A seconda dello strumento di import utilizzato e delle opzioni scelte potrebbe essere necessario adattare leggermente le query, ad esempio se NON si è scelto di trasformare i nomi campo in minuscolo occorre rinominare i nomi  campo.***

 

```sql
SELECT *
FROM dati.province;
```



## La clausola WHERE

La **clausola WHERE del linguaggio SQL** permette di scrivere **query** per visualizzare soltanto le righe di una tabella che rispettano una  particolare condizione. 

La clausola WHERE deve essere sempre scritta dopo la clausola FROM, mai prima.

### ESERCIZIO 3b - prima WHERE

- proviamo a ricercare solo la provincia di Torino.

```sql
SELECT *
FROM dati.province
WHERE provincia = 'Torino'
```

- limitiamo il numero di campi visualizzati.

```sql
SELECT fid, provincia, sigla
FROM dati.province
WHERE provincia = 'Torino'
```

DOMANDA: come si presenta il campo delle geometrie?


 <img title="" src="05_cenni_di_SQL.assets/geometriewkb.png" alt="WKB" data-align="inline">

Posso utilizzare le più comuni **condizioni di eguaglianza o diseguaglianza**:

- "="			uguale

- "<"			minore

- ''>"			maggiore

- "<>" o "!="	    diverso

  

### ESERCIZIO 3c - Gli altri operatori

*Esempio: Cerca tutte le province con are superiore a 5'000 Km2*

```sql
SELECT *
FROM dati.province
where shape_area > 5000000000
```

Per le stringe sono presenti altri operatori che possono essere particolarmente utili: LIKE e ILIKE

*Esempio: Cerca tutti i comuni Piemontesi il cui nome inizia con "San"* 

```sql
SELECT   *
FROM dati.comuni
where comune_nom ILIKE 'San%'
```

Diverse condizioni possono essere concatenate utilizzando i classici operatori Booleani AND, OR, XOR 

*Esempio: estrai tutti i comuni in provincia di Torino più quelli in provincia di Biella*

```sql
SELECT   *
FROM dati.comuni
where zona_alt = 'C' or zona_alt = 'M'
```

Quando devo concatenare più condizioni OR è possibile utilizzare la condizione IN()

*Esempio: nel caso precedente posso riscrivere la query in questo modo*

```sql
SELECT   *
FROM dati.comuni
where zona_alt in ('C','M');

-- E alterativa a questa sintassi

SELECT   *
FROM dati.comuni
where zona_alt ='C' OR zona_alt ='M';
```



## La clausola JOIN

Una delle principali caratteristiche dei database relazionali è poter mettere in "relazione" gli oggetti contenuti nelle tabelle. La clausola JOIN pemette di eseguire quest'operazione. Per poter effettuare un JOIN **devo avere un campo che permetta di mettere le tabelle in relazione tra loro**.

Esistono diverti tipi di join:

- inner JOIN (o semplicemente JOIN)
- outer JOIN (left JOIN o right JOIN)
- full JOIN (o full outer JOIN)

![](05_cenni_di_SQL.assets/2.png)

**Sintassi**

```sql
SELECT column_name(s)
FROM table1
[LEFT/RIGHT/FULL] JOIN table2
ON table1.column_name = table2.column_name
[WHERE condition];
```

### ESERCIZIO 4 - JOIN

- da DBManager o da PGAdmin eseguite questa query

```sql
SELECT
  localita.fid,
  localita.loc2011,
  localita.pro_com,
  localita.denominazi as nome,
  localita.tipo_loc,
  tl.descrizione
FROM
  dati.localita
JOIN dati.tipo_localita as tl ON localita.tipo_loc = tl.tipo_loc::numeric;
```
In alternativa alla clausola **inner JOIN**  può essere utilizzata la clausola WHERE.
```  sql
SELECT
  localita.fid,
  localita.geom,
  localita.loc2011,
  localita.pro_com,
  localita.tipo_loc,
  localita.denominazi AS nome,
  tl.descrizione
FROM
  dati.localita,
  dati.tipo_localita AS tl
WHERE
  localita.tipo_loc = tl.tipo_loc::numeric;
```

*Le due query restituiscono lo stesso risultato ?*

Ci sono alcune novità:

- **localita.denominazi as nome** con **AS** è possibile dare un _alias_ al nome del campo
- allo stesso modo sempre con **AS** è possibile dare un _alias_ al nome della tabella **dati.tipo_localita as tl**
- con **::** facciamo il _cast_ da un formato di campo ad un altro **tl.tipo_loc::numeric** https://www.educba.com/cast-in-postgresql/
  altre modalità di conversione sono le funzioni **to_XXXXXX** https://www.techonthenet.com/postgresql/functions/index_alpha.php
  localita.tipo_loc = to_number(tl.tipo_loc,'99')

Il _join_ è stato realizzato con la clausola **WHERE** che letteralmente unisce le due tavole dove la colonna  "TIPO_LOC" della tavola **localita** è uguale alla colonna **tipo_loc** - opportunamente trasformata - della tavola **t1** ovvero **dati.tipo_localita**

Come abbiamo detto esistono diversi tipi di JOIN. Vediamoli nel dettaglio:

### INNER JOIN

Si tratta del caso visto in precedenza. Possiamo immaginare l’operazione di INNER JOIN utilizzando una  rappresentazione insiemistica, in cui le righe delle tabelle sono  collegate attraverso l’uguaglianza di valori, che corrisponde  all’intersezione:

![](05_cenni_di_SQL.assets/innerjoin.png)

In pratica verranno restituite **SOLO le righe** per cui i valori contenuti nel campo della tabella di sinistra sono presenti anche nella tabella di destra, e viceversa.

### LEFT (OUTER) JOIN

Nel caso della LEFT JOIN mi verranno restituiti **sempre  TUTTI i record della tabella di sinistra** con abbinate le righe della tavola di destra per cui esiste una corrispondenza; nel caso la corrispondenza non esista i campi rimarranno vuoti.

N.B. La clausola OUTER è opzionale  **LEFT JOIN** è uguale a **LEFT OUTER JOIN** quindi puoi usarli in modo intercambiabile.

![](05_cenni_di_SQL.assets/leftouterjoin.png)


ATTENZIONE: invertendo l'ordine delle tavole  (tavola **LEFT** e tavola **RIGHT**) il risultato cambia!

Se voglio trovare SOLO gli oggetti (le righe) della tavola di sinistra che NON HANNO corrispondenza nella tavola di destra devo aggiungere una condizione WHERE [*campo di join della tavola destra*] IS NULL 

![](05_cenni_di_SQL.assets/leftouterjoin_olt.png)




### RIGHT JOIN

E' la funzione simmetrica della LEFT JOIN. In questo caso vengono restituiti TUTTI gli oggetti (le righe) della tabella di destra con a abbinate le righe della tavola di sinistra per cui esiste una corrispondenza; nel caso la corrispondenza non esista i campi rimarranno vuoti.



![](05_cenni_di_SQL.assets/rightouterjoin.png)


Se voglio trovare SOLO gli oggetti della tavola di destra che NON HANNO corrispondenza nella tavola di sinistra devo aggiungere una condizione WHERE [*campo di join della tavola sinistra*] IS NULL 



![](05_cenni_di_SQL.assets/rightouterjoin_ort.png)



Nota che **RIGHT JOIN** è uguale a **RIGHT OUTER JOIN** quindi puoi usarli in modo intercambiabile.



### ESERCIZIO 4b:

Ho due layer con le stazioni meteo del Piemonte; i dataset però non coincidono, in ognuna manca un certo numero di stazioni rispetto all'altra. 

- Quali sono le stazioni mancanti in staz_2 rispetto al layer staz_1 ?
- Quali sono le stazioni mancanti nel staz_1 rispetto al layer staz_2 ?

```sql
-- Esempio di LEFT JOIN
SELECT a.a_meteo_staz as stazione_1, b.a_meteo_staz as stazione_2, a.geom
FROM dati.staz_1 as a
LEFT JOIN dati.staz_2 as b USING(a_meteo_staz)
WHERE b.a_meteo_staz is null

-- Esempio di RIGHT JOIN
SELECT a.a_meteo_staz as stazione_1, b.a_meteo_staz as stazione_2, b.geom
FROM dati.staz_1 as a
RIGHT JOIN dati.staz_2 as b USING(a_meteo_staz)
WHERE a.a_meteo_staz is null
```



### FULL (OUTER) JOIN

L’operazione di FULL (OUTER) JOIN restituisce tutti i record di entrambe le tabelle e abbina le righe per cui esiste una corrispondenza.  Possiamo dire che la FULL JOIN è l’unione tra la LEFT e RIGHT JOIN.  Anche in questo caso OUTER è opzionale.

![](05_cenni_di_SQL.assets/fullouterjoin.png)



Anche in questo caso posso escludere la parte comune con una condizione WHERE

![](05_cenni_di_SQL.assets/fullouterjoin_bt.png)


Nota che **FULL JOIN** è uguale a **FULLOUTER JOIN** quindi puoi usarli in modo intercambiabile.



### ESERCIZIO 4c:

Posso unire nella stessa tavola i due dataset precedenti  

```sql
--FULL OUTER JOIN 
SELECT a.a_meteo_staz as stazione_1,a.strumento as strumento_1, b.a_meteo_staz as stazione_2, b.strumento as strumento_2, a.geom
FROM dati.staz_1 as a
FULL JOIN dati.staz_2 as b USING(a_meteo_staz)
WHERE a.a_meteo_staz is null OR b.a_meteo_staz is null

--FULL OUTER JOIN - only row unique to both table
SELECT a.a_meteo_staz as stazione_1,a.strumento as strumento_1, b.a_meteo_staz as stazione_2, b.strumento as strumento_2, a.geom
FROM dati.staz_1 as a
FULL JOIN dati.staz_2 as b USING(a_meteo_staz)
WHERE a.a_meteo_staz is null OR b.a_meteo_staz is null
```



Da adesso in poi non potrete più dimenticare cosa significano le differenti **JOIN**!

![](05_cenni_di_SQL.assets/joinmeme.png)



## La clausola CREATE

Grazie al comando create è possibile creare nuovi oggetti nel database. Abbiamo già incontrato questo comando per creare nuove tabelle da zero.

```sql
CREATE TABLE table_name(
    id SERIAL,
    name VARCHAR NOT NULL
);
```

 Nel nostro caso possiamo utilizzarlo per salvare nel database i risultati della nostra interrogazione.

### ESERCIZIO 5 - Salvare i risultati di una query nel database

```sql
CREATE TABLE dati.localita_decodifica AS  
SELECT
  localita.fid,
  localita.geom,
  localita.loc2011,
  localita.pro_com,
  localita.tipo_loc,
  localita.denominazi AS nome,
  tl.descrizione
FROM
  dati.localita,
  dati.tipo_localita AS tl
WHERE
  localita.tipo_loc = tl.tipo_loc::numeric;
```

Grazie al DB manager è possibile visualizzare i risultati di un'interrogazione creando un layer virtuale, senza dover salvare i dati nel database.

![image-20220201224006412](05_cenni_di_SQL.assets/image-20220201224006412.png)



## Le clausole TRUNCATE DELETE e DROP

il comando TRUNCATE permette di eliminare tutti i dati di una tabella

```sql
TRUNCATE TABLE dati.localita_decodifica;
```
Posso eliminare un subset di righe utilizzando il comando DELETE
```sql
DELETE
FROM dati.localita_decodifica
WHERE tipo_localita =1;
```

Posso eliminare l'oggetto tabella con il comando DROP
```sql
DROP dati.localita_decodifica;
```



## Le clausola INSERT

Tramite il comando INSERT  posso aggiungere singole righe in una tabella.

```sql
INSERT INTO table_name [column1,column2] VALUES (valore1a, valore2b);
INSERT INTO table_name [column1,column2] VALUES (valore2a, valore2b);
INSERT INTO table_name [column1,column2] VALUES (valore3a, valore2b);
```

Possiamo inserire direttamente anche i risultati derivanti da una query in una tavola preesistente. Se la tabella esiste posso modificare la query dell'esercizio 5   in questo modo:

```sql
TRUNCATE TABLE dati.localita_decodifica;
INSERT INTO dati.localita_decodifica  
SELECT
  localita.fid,
  localita.geom,
  localita.loc2011,
  localita.pro_com,
  localita.tipo_loc,
  localita.denominazi AS nome,
  tl.descrizione
FROM
  dati.localita,
  dati.tipo_localita AS tl
WHERE
  localita.tipo_loc = tl.tipo_loc::numeric;
```



## Le funzioni

PostgreSQL mette a disposizione un elevato numero di funzioni predefinite. Le funzioni principali sono molto simili a quelle messa disposizione da un comune foglio di calcolo e permettono di effettuare operazioni su numeri, stringhe di testo, date  e altri tipi di dati.

Per avere una panoramica delle funzioni disponibili si può fare riferimento alla documentazione ufficiale di PostgreSQL:

- Stringhe: https://www.postgresql.org/docs/14/functions-string.html
- Funzioni matematiche e operatori: https://www.postgresql.org/docs/14/functions-math.html
- Date: https://www.postgresql.org/docs/14/functions-datetime.html

Ogni funzione richiede un determinato numero e tipo di parametri per funzionare. Ad esempio per la funzione la funzione left la documentazione mi dice:

`left` ( *`string`* `text`, *`n`* `integer` ) → `text`

Returns first *`n`* characters in the string, or when *`n`* is negative, returns all but last |*`n`*| characters.

`left`('abcde', 2) → `ab`

ESEMPIO: voglio ricavare il codice ISTAT della provincia dal codice completo (provincia+comune)

```sql
SELECT
comune_nom,
left(comune_ist,3) as cod_provincia,
right(comune_ist,3) as cod_comune
FROM dati.comuni
order by 2,1;
```

 Attenzione: ho usato la clausola AS per rinominare i campi nella tabella di output.



## La clausola GROUP BY

La clausola **GROUP BY** di **SQL** è utilizzata nelle operazioni di **SELECT** al fine di raggruppare i valori identici presenti in una o più colonne. La clausola *GROUP BY* è un'accessorio facoltativo di *SELECT* e va inserita dopo *FROM* o *WHERE* (se presente).

```sql
SELECT <elenco campi>
FROM <tabella>
WHERE <condizione>
GROUP BY <elenco campi>
```

Nella sua forma di utilizzo più semplice la clausola *GROUP BY* produce un risultato analogo a [SELECT DISTINCT](https://www.mrw.it/sql/utilizzare-select-distinct-eliminare-doppioni_6851.html).

Quali sono le categorie possibili per il campo  d_zona_alt (zona altimetrica) dei comuni piemontesi ?

```sql
SELECT d_zona_alt 
FROM dati.comuni
GROUP BY d_zona_alt;

SELECT DISTINCT d_zona_alt FROM dati.comuni;
```

Insieme a GROUP BY è possibile utilizzare molte funzioni di aggregazione, trovate l'elenco completo qui:

https://www.postgresql.org/docs/14/functions-aggregate.html

Alcuni esempi:

| Function              Description                            |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `array_agg` ( `anynonarray` ) → `anyarray`                   | Collects all the input values, including nulls, into an array. |
| `avg` ( `smallint` ) → `numeric`                             | Computes the average (arithmetic mean) of all the non-null input values. |
| `count` ( `*` ) → `bigint`                                   | Computes the number of input rows.                           |
| `count` ( `"any"` ) → `bigint`                               | Computes the number of input rows in which the input value is not null. |
| `max` ()                                                     | Computes the maximum of the non-null input values. Available for any numeric, string, date/time, or enum type, as well as `inet`, `interval`, `money`, `oid`, `pg_lsn`, `tid`, and arrays of any of these types. |
| `min` ()                                                     | Computes the minimum of the non-null input values. Available for any numeric, string, date/time, or enum type, as well as `inet`, `interval`, `money`, `oid`, `pg_lsn`, `tid`, and arrays of any of these types. |
| `string_agg` ( *`value`* `text`, *`delimiter`* `text` ) → `text` | Concatenates the non-null input values into a string. Each value after the first is preceded by the corresponding *`delimiter`* (if it's not null). |
| `sum` (  )                                                   | Computes the sum of the non-null input values.               |

Sono disponibili anche funzioni statistiche più complesse, potete trovare l'enenco qui: https://www.postgresql.org/docs/14/functions-aggregate.html

### ESERCIZIO 6 - GROUP BY

- Contare quanti comuni ci sono per ogni provincia del Piemonte?
- Qual'è la popolazione totale per provincia ?
- Qual'è la provincia con quota media dei comuni più alta ?
- Quale la provincia con il maggior numero di comuni ? Quella con il numero minore ?

```sql
SELECT
left(comune_ist,3) as cod_provincia,
count(*) as num_comuni,
min(quota_min),
max(quota_max),
avg(quota_medi),
sum(pop_2011) as pop_totale
FROM dati.comuni
GROUP BY left(comune_ist,3)
order by 1;
```

**ATTENZIONE: **

- **quando fate una GROUP BY potete selezionare solo i campi presenti nella clausola GROUP BY o inseriti all'interno di una funzione di aggregazione**
- **Il raggruppamento può avvenire anche sulla base di più campi**



Grazie all'utilizzo della JOIN, vista in precedenza posso collegare il codice ISTAT della provincia con il nome della provincia:

```sql
select
	p.provincia,
	count(*) as num_comuni,
	min(quota_min),
	max(quota_max),
	avg(quota_medi),
	sum(pop_2011) as pop_totale
	FROM dati.comuni
	LEFT join dati.province p on
	p.cod_pro = left(comune_ist,3)::integer
	GROUP by p.provincia ORDER by 1;
```



## La clausola WITH

in SQL è possibile scrivere query annidate, il risultato di una query fa da input per un'altra query

ESEMPIO: quali sono i comuni più popolosi per ogni provincia ?

```sql
SELECT comune_nom,pop_2011 FROM
	(
    SELECT left(comune_ist,3),max(pop_2011) as pop
    FROM
    dati.comuni c
    GROUP BY left(comune_ist,3)
	) a
JOIN dati.comuni ON pop=pop_2011
ORDER BY pop_2011 desc;


```

Utilizzando la clausola WITH la sintassi diventa più semplice:

```sql
WITH a AS (
SELECT left(comune_ist,3),max(pop_2011) as pop
FROM
dati.comuni c
GROUP BY left(comune_ist,3)
)
SELECT comune_nom,pop_2011 FROM a
JOIN dati.comuni ON pop=pop_2011
ORDER BY pop_2011 desc
```

