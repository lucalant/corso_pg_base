---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio-Febbraio, 2025
keywords: [PostgreSQL, PostGIS, Open Source, GFOSS]
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">


# Importare dati nel dabatase

Per importare i dati in PostgreSQL possiamo usare differenti strumenti. Ognuno ha differenti caratteristiche e specificità.

- Processing di QGIS
- Drag&Drop da QGIS
- DB Manager di QGIS
- shp2pgsql-gui
- ogr2ogr



## Processing di QGIS

Lavorando con i dati geografici il principale strumento di import dei dati è 

   <img title="" src="03_caricareidati.assets/processing.png" alt="caricare dati con processing" data-align="inline">


E' necessario indicare i riferimenti alla connessione ad un database:
- il nome della connessione fornita in QGIS: host, user, port e password;
- lo schema;
- nome della tavola in output (di default propone il nome del layer);
- la colonna del campo chiave;
- nome della colonna che conterrà le geometrie (geom o the_geom ma non darlo mai per scontato);
- encoding il set di caratteri di riferimento.

 

Più altre opzioni:

- si desidera sovrascrivere un layer con lo stesso nome eventualmente presente nello schema?

- la creazione di un indice spaziale (IMPORTANTE);

- campi in minuscolo (RACCOMANDATISSIMO);

- elimina la lunghezza per i campi stringa;

- geometrie multipart o singlepart.

  

---

## Drag & Drop di QGIS

Questo è tecnicamente più facile ma bisogna configurare una connessione a Postgres/PostGIS.
La connessione può essere fatta sia dal pannello *Browser* che dal *Data Source Manager*

<img title="" src="03_caricareidati.assets/qgis_pg_connection.png" alt="connessione QGIS" data-align="inline">

---

## DB Manager di QGIS

Anche questo è molto semplice sfruttando la connessione a Postgres/PostGIS e il DB Manager

<img title="" src="03_caricareidati.assets/qgis_import_dbmanager.png" alt="import DB Manager QGIS" data-align="inline">

---

## shp2pgsql-gui

Normalmente è un programma già installato con Postgres/PostGIS oppure lo si installa con i packages di PostGIS su Linux.

Permette sia l'import che l'export dei dati da Postgres, configurando la connessione

<img title="" src="03_caricareidati.assets/shp2pggui_connection.png" alt="connessione a PG" data-align="inline">

Dall'interfaccia è possibile gestire sia l'import

<img title="" src="03_caricareidati.assets/shp2pggui_import.png" alt="shp2pggui import" data-align="inline">

che l'export

<img title="" src="03_caricareidati.assets/shp2pggui_export.png" alt="shp2pggui export" data-align="inline">

---

## ogr2ogr

ESEMPIO:

```bash
#windows
ogr2ogr -lco overwrite=YES -lco SCHEMA=dati -f "PostgreSQL" PG:"host=localhost user=rokko port=5433 dbname=masterpadova password=aaa" "d:\dati\province.shp" -skipfailures -a_srs "EPSG:32632" -nln province_ogr -nlt PROMOTE_TO_MULTI

#linux
ogr2ogr -lco overwrite=YES -lco SCHEMA=dati -f "PostgreSQL" PG:"host=localhost user=test password=test port=5432 dbname=lucalant" "/home/lucalant/LACIE/lavori/luk/Corsi/2025_master_PG_Padova/dati/province.shp" -skipfailures -a_srs "EPSG:32632" -nln province_ogr -nlt PROMOTE_TO_MULTI
```

https://www.gdal.org/ogr2ogr.html

https://www.bostongis.com/PrinterFriendly.aspx?content_name=ogr_cheatsheet

https://www.gdal.org/ogr_formats.html (42 formati gestiti)

ogr2ogr è molto utile quando si caricano dati che cambiano frequentemente per esempio dati che arrivano da strumenti in vari formati.
Oppure quando si vuole spostare dei dati da un database ad un altro.

---

## Esercitazione

### I dati

Dati ISTAT (http://www.istat.it/it/archivio/104317)

- Province.zip: Contiene i limiti provinciali del Piemonte in formato shapefile
- Datigeo.gpkg: contiene 4 layer
   - Comuni del Piemonte
   - Province del Piemonte
   - Località (poligonale)
   - Zone di allerta

- tipo_localita.csv:  Tabella di decodifica TIPO_LOC Tipologia di località 2001/2011. Il campo può assumere i seguenti valori: 
    1. centro abitato 
    2. nucleo abitato 
    3. località produttiva 
    4. case sparse

Da Geoportale Regione Piemonte (http://www.geoportale.piemonte.it/cms/)

- Autostrade.zip: una selezione della viabilità principale (autostrade lineare) in formato shapefile
- Staz.json: anagrafica delle stazioni idrometeorologiche di Arpa Piemonte

I dati sono disponibili nella cartella **dati esercitazione**  

---

### ESERCIZIO 1 - caricare i dati in Postgres

- creare una connessione al database in QGIS
- creare lo schema **dati** utilizzando il Database Manager oppure da PGAdmin

<img title="" src="03_caricareidati.assets/dbmanager.png" alt="dbmanager QGIS" data-align="inline">

- provare a caricare i dati con i differenti metodi: DB Manager, Drag&Drop, shp2pqsql-gui, ogr2ogr

DOMANDA: i dati caricati con i differenti metodi sono uguali? In cosa differiscono?

---

### ESERCIZIO 2 - caricare una tabella non geografica

- Utilizzando DBManager oppure Processing oppure shp2pgsql-gui caricare la tabella **tipo_localita.csv** nello schema **dati**

- provare anche la strada alternativa di creazione della tabella in DB Manager
-- campi da creare: tipo_loc integer e descrizione char(30) o varchar(30)

<img title="" src="03_caricareidati.assets/dbmanager_table.png" alt="dbmanager table QGIS" data-align="inline">

---







