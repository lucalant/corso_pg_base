---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio-Febbraio, 2025
keywords: [PostgreSQL, PostGIS, Open Source, GFOSS]

---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

## Presentazione - Chi siamo

**Luca Lanteri**: geologo, amante da sempre della montagna della neve e di tutto quello che supera i 2000m di quota. Lavoro in Arpa da ormai 20 anni, dove coordino il *"Nucleo Operativo Studi e Monitoraggi Geologici"* che si occupa di neve e valanghe, della gestione dei rischi naturali e del monitoraggio dei fenomeni franosi. Utilizzo quotidianamente strumenti Open Source in diversi ambiti del mio lavoro. 

**Rocco Pispico**: informatico, lavoro da 30 anni con dati territoriali relativi alla geologia. Inizio con lo sviluppo dei primi GIS *fatti in casa* per poi passare allo sviluppo con strumenti ESRI. 12 anni fa approdo per necessità al software *Open Source* e ogni giorno mi stupisco di quante funzionalità si aprano uscendo da un mondo proprietario.   

# Programma del corso

## Capitolo 1 - Installazione

- installazione LINUX
- installazione WINDOWS
- installazione MAC
- configurazione: postgres.conf e pg_hba.conf

## Capitolo 2 - Introduzione

- Cos'è PostgreSQL e la sua storia
- Perché usare PG: principali caratteristiche e confronto con altre soluzioni di GeoDB (Sqlite/spatialite)
- Installazione PG (win/linux)
- Cos'è PostGIS
- Com'è organizzato PG: Cluster -> Database -> Schemi -> Tavole
- Principali strumenti di amministrazione per PostgreSQL (Psql - PgAdmin - DBeaver - PhpPgadmin)
- Introduzione alla gestione degli utenti e dei ruoli

## Capitolo 3 - Importare i dati

- processing
- shp2pgsql-gui
- ogr2ogr
- drag & drop
- DB Manager

## Capitolo 4 - Gestire i dati 

- Tipi di dati di PostgreSQL
- Tipi di dati spaziali in PostGIS
- Indici spaziali
- Chiavi primarie
- Sequence 
- NULL


## Capitolo 5 - Introduzione al linguaggio SQL
- Sintassi specifica di PostgreSQL
- SELECT
- JOIN
- CREATE
- TRUNCATE DELETE e DROP 
- INSERT
- Funzioni 
- GROUP BY
- WITH

## Capitolo 6 - Analisi spaziali parte 1 
- Centroidi: ST_Centroid e ST_PointOnSurface
- ST_Intersects
- Conteggi
- Trasformazioni
- Nuove geometrie

## Capitolo 7
- amministrazione avanzata degli utenti
- cenni di amministrazione di un database aziendale 
