---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: Gennaio/Febbraio, 2025
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

# Corso PostgreSQL / PostGIS



## Creare e gestire gli utenti 

In postgreSQL come in tutti i database che si rispettano è possibile gestire le autorizzazioni ai principali oggetti del database. Possono essere forniti i diritti a singoli utenti (**USERS**) o a gruppi (**ROLES**). I gruppi (ROLES) sono dei contenitori di utenti e permettono di assegnare gli stessi GRANT a più utenti contemporaneamente. 

Ogni oggetto ha differenti tipi di diritti:

**DATABASE**: Create, Temp, Connect
**SCHEMA**: Usage, Connect
**TABLE, VIEW**: Insert, Select, Update, Delete, Truncate, Trigger, References
**FUNCTION**: Execute

Di default esiste solo l’utente postgres che è superutente. Si può usare questo utente per amministrare il database oppure crearne un altro con minori privilegi.

L'utente postgres deve essere utilizzato unicamente per effettuare operazioni di amministrazione sul database.  

PostgreSQL distingue due oggetti differenti utenti (USERS) e ruoli (ROLES) In realtà gli users sono semplicemente dei ruoli che hanno diritto di login.

```sql
CREATE USER luca PASSWORD 'pippo';
--equivale a:
CREATE ROLE utente PASSWORD '***';
```

Posso modificare le proprietà di un utente dopo averlo creato con il comando ALTER.  Ad esempio per cambiare o aggiungere la password in un secondo momento:

```sql
ALTER ROLE luca password 'pluto';
```

Per rimuoverlo:

```sql
DROP ROLE utente;
```

E' possibile creare dei gruppi (roles) a cui vengono assegnati gli utenti

```sql
CREATE ROLE gruppo;
```

Si aggiungono (e tolgono) gli utenti al gruppo:

```sql
GRANT ADMIN TO luca, rocco;
REVOKE ADMIN FROM rocco;
```

ATTENZIONE: PostgreSQL usa un utente speciale `PUBLIC` che serve per definire l'accesso a tutti gli utenti



## Proprietario (OWNER)

Quando un oggetto viene creato, gli viene assegnato un **proprietario**. Il proprietario è normalmente il ruolo che ha eseguito l'istruzione di creazione. Per la maggior parte dei tipi di oggetti, lo stato iniziale è che <u>solo il proprietario (o un superutente) può fare qualsiasi cosa con l'oggetto</u>. Per consentire ad altri ruoli di utilizzarlo, è necessario concedere i privilegi.

Un oggetto può essere assegnato a un nuovo proprietario con un comando ALTER del tipo appropriato per l'oggetto, ad esempio

```sql
ALTER TABLE dati.comuni OWNER TO rocco;
```



## I privilegi disponibili

| PRIVILEGI | DESCRIZIONE                                                  |
| --------- | ------------------------------------------------------------ |
| SELECT    | Consente di fare selezioni da qualsiasi colonna, o colonne specifiche, di una tabella, vista, vista materializzata o altro oggetto simile a una tabella. |
| INSERT    | Consente di inserire una nuova riga in una tabella, vista, ecc. Può essere concesso su colonne specifiche, nel qual caso solo quelle colonne possono essere assegnate nel comando INSERT (le altre colonne riceveranno quindi valori predefiniti). |
| UPDATE    | Consente di aggiornare qualsiasi colonna, o colonna o colonne specifiche, di una tabella, vista, ecc. (In pratica, qualsiasi comando UPDATE non banale richiederà anche il privilegio SELECT, poiché deve fare riferimento alle colonne della tabella per determinare quali righe aggiornare e /o per calcolare nuovi valori per le colonne.). Per le sequenze, questo privilegio consente l'uso delle funzioni nextval e setval. Per oggetti di grandi dimensioni, questo privilegio consente di scrivere o troncare l'oggetto. |
| DELETE    | Consente di eliminare una riga da una tabella, vista, ecc. (In pratica, qualsiasi comando DELETE non banale richiederà anche il privilegio SELECT, poiché deve fare riferimento alle colonne della tabella per determinare quali righe eliminare.) |
| TRUNCATE  | permette di eliminare TUTTE le righe di una tabella. Il TRUNCATE di una vista non ha senso. |
| REFERENCE | Consente la creazione di un vincolo di chiave esterna che fa riferimento a una tabella o a colonne specifiche di una tabella. |
| TRIGGER   | Consente la creazione di un trigger su una tabella, vista, ecc. |
| CREATE    | Per i **database**, consente la creazione di nuovi schemi e pubblicazioni all'interno del database e consente l'installazione di estensioni attendibili all'interno del database. Per gli SCHEMI, consente di creare nuovi oggetti all'interno dello schema. Per rinominare un oggetto esistente, è necessario possedere l'oggetto e disporre di questo privilegio per lo schema contenitore. Per i TABLESPACE, consente di creare tabelle, indici e file temporanei all'interno del tablespace e consente la creazione di database che hanno il tablespace come tablespace predefinito. |
| CONNECT   | Consente al beneficiario di connettersi al database. Questo privilegio viene verificato all'avvio della connessione (oltre a verificare eventuali restrizioni imposte da pg_hba.conf). |
| TEMPORARY | Consente la creazione di tabelle temporanee durante l'utilizzo del database. |
| EXECUTE   | Consente di chiamare una funzione o una procedura, incluso l'uso di tutti gli operatori implementati sulla funzione. Questo è l'unico tipo di privilegio applicabile a funzioni e procedure. |
| USAGE     | Per i **linguaggi procedurali**, consente l'uso del linguaggio per la creazione di funzioni in quel linguaggio. Questo è l'unico tipo di privilegio applicabile ai linguaggi procedurali.<br />Per gli **schemi**, consente l'accesso agli oggetti contenuti nello schema (supponendo che siano soddisfatti anche i requisiti di privilegio degli oggetti). In sostanza, ciò consente al beneficiario di "cercare" gli oggetti all'interno dello schema. Senza questa autorizzazione, è ancora possibile vedere i nomi degli oggetti, ad esempio interrogando i cataloghi di sistema. Inoltre, dopo aver revocato questa autorizzazione, le sessioni esistenti potrebbero avere istruzioni che hanno precedentemente eseguito questa ricerca, quindi questo non è un modo completamente sicuro per impedire l'accesso agli oggetti.<br><br>Per le **SEQUENCE**, consente l'uso delle funzioni currval e nextval. |
|           |                                                              |



## Esercizio: 

Ipotizziamo l'amministrazione di un database con tre tipi di profili differenti:  

1. L'amministratore del database> admin 
2. L'applicazione con un accesso completo per i suoi dati> read_write 
3. L'accesso in sola lettura> read_only 

```sql
-- CREO GLI UTENTI
CREATE USER admin with password 'pw1';
CREATE USER user_r with password 'pw2';
CREATE USER user_rw with password 'pw3';
```

Limitiamo la possibilità di accesso allo schema dati:

```SQL
-- ACCESS SCHEMA
REVOKE ALL     ON SCHEMA dati FROM PUBLIC;
GRANT  USAGE   ON SCHEMA dati  TO user_r;
GRANT  USAGE   ON SCHEMA dati  TO user_rw;
```

 La prossima serie di query revoca tutti i privilegi  agli utenti non autenticati e fornisce un insieme limitato di privilegi  per l'utente `read_write`. 

```sql
-- ACCESS TABLES
REVOKE ALL ON ALL TABLES IN SCHEMA dati FROM PUBLIC ;
GRANT SELECT                         ON ALL TABLES IN SCHEMA dati TO user_r;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA dati TO user_rw;
GRANT ALL                            ON ALL TABLES IN SCHEMA dati TO admin;

-- ACCESS SEQUENCES
REVOKE ALL   ON ALL SEQUENCES IN SCHEMA dati FROM PUBLIC;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA dati TO user_r; -- allows the use of CURRVAL
GRANT UPDATE ON ALL SEQUENCES IN SCHEMA dati TO user_rw; -- allows the use of NEXTVAL and SETVAL
GRANT USAGE  ON ALL SEQUENCES IN SCHEMA dati TO user_rw; -- allows the use of CURRVAL and NEXTVAL
GRANT ALL    ON ALL SEQUENCES IN SCHEMA dati TO admin;
```



## Concedere privilegi di accesso agli oggetti creati in futuro

Con le query sottostanti, è possibile impostare i privilegi di accesso  sugli oggetti creati in futuro nello schema specificato. 

```sql
ALTER DEFAULT PRIVILEGES IN SCHEMA dati GRANT SELECT                      ON TABLES TO user_r;
ALTER DEFAULT PRIVILEGES IN SCHEMA dati GRANT SELECT,INSERT,DELETE,UPDATE ON TABLES TO user_rw;
ALTER DEFAULT PRIVILEGES IN SCHEMA dati GRANT ALL                         ON TABLES TO admin;
```

 Oppure, è possibile impostare i privilegi di accesso sugli oggetti creati in futuro dall'utente specificato. 

```sql
ALTER DEFAULT PRIVILEGES FOR ROLE admin GRANT SELECT ON TABLES TO user_r;
```



Tutte le operazioni possono essere effettuate ache da PgAdmin4

In generale si accede ali privilegi degli oggetti utilizzando la funzione tasto dx -> proprietà dall'oggetto interessato.

![image-20220211082329897](07_gestire_gli_utenti.assets/image-20220211082329897.png)



## Usare i Roles

Nel caso precedente ho assegnato i diritti direttamente agli utenti. Nel caso il mio numero di utenti sia elevato è consigliabile gestire i permessi tramite i ruoli (roles in postgresql).

```sql
-- CREO I RUOLI
CREATE USER grp_admin; 
CREATE USER grp_user_r;
CREATE USER grp_user_rw;
```

Limitiamo la possibilità di accesso allo schema dati agli utenti a cui ho dato precedentemente i privilegi:

```SQL
-- ACCESS SCHEMA
REVOKE ALL     ON SCHEMA dati FROM admin;
REVOKE ALL     ON SCHEMA dati FROM user_r;
REVOKE ALL     ON SCHEMA dati FROM user_rw;

REVOKE ALL ON ALL TABLES IN SCHEMA dati FROM PUBLIC user_r ;
REVOKE ALL ON ALL TABLES IN SCHEMA dati FROM PUBLIC user_rw ;
```

Ora fornisco l'accesso ai gruppi

```sql
GRANT  USAGE   ON SCHEMA dati  TO grp_user_r;
GRANT  USAGE   ON SCHEMA dati  TO grp_user_rw;
GRANT  USAGE   ON SCHEMA dati  TO grp_admin;
```

Devo aggiungere gli utenti ai gruppi

```sql
GRANT grp_user_r to user_r;
GRANT grp_user_rw to user_rw;
GRANT grp_admin to admin;

```

Provate a vedere da QGIS, creando una nuova connessione con un utente differente cosa riuscite a vedere e su quali tabelle riuscite a scrivere.

